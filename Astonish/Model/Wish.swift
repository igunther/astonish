//
//  Wish.swift
//  Astonish
//
//  Created by Øystein Günther on 01/02/2021.
//

import SwiftUI
import FirebaseFirestoreSwift
import FirebaseFirestore

struct Wish: Identifiable, Codable {
    @DocumentID var id: String? = UUID().uuidString
    let wishListId: String
    let name: String
    let note: String?
    let url: String?
    //@ServerTimestamp var createdAt: Timestamp?
    //var updatedAt: Timestamp? // Set it to nil in order for the value to be updated automatically by the server.
    
    init(wishListId: String, name: String, note: String?, url: String?) {
        self.wishListId = wishListId
        self.name = name
        self.note = note
        self.url = url
    }
    
    init(id: String, wishListId: String, name: String, note: String?, url: String?) {
        self.wishListId = wishListId
        self.id = id
        self.name = name
        self.note = note
        self.url = url
    }
}
