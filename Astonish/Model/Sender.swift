//
//  Sender.swift
//  Astonish
//
//  Created by Øystein Günther on 28/01/2021.
//

import SwiftUI
import Firebase

struct Sender: Codable, Hashable, Identifiable {
    let id: String
    let firstName: String
    let lastName: String?
    
    var fullName: String {
        get {
            let firstName = self.firstName
            let lastName = self.lastName == nil ? "" : self.lastName!
                    
            if firstName != "" && lastName != "" {
                return firstName + " " + lastName
            } else if firstName == "" {
                return lastName
            }
            return firstName
        }
    }
    
    init(id: String, firstName: String, lastName: String?) {
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
    }
    
    init(snapshot: DocumentSnapshot) {
        self.id = snapshot.get(DOCUMENT_PERSON_FIELDNAME_ID) as? String ?? ""
        self.firstName = snapshot.get(DOCUMENT_PERSON_FIELDNAME_FIRST_NAME) as? String ?? ""
        self.lastName = snapshot.get(DOCUMENT_PERSON_FIELDNAME_LAST_NAME) as? String ?? ""
    }
}
