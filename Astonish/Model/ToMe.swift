//
//  ToMe.swift
//  Astonish
//
//  Created by Øystein Günther on 27/01/2021.
//

import SwiftUI
import FirebaseFirestoreSwift
import FirebaseFirestore

struct ToMe: Identifiable, Codable {
    @DocumentID var id: String? = UUID().uuidString
    let uid: String
    let occasion: Occasion
    let name: String
    @ExplicitNull var sender: Sender?
    @ExplicitNull var senders: [String]?
    var quantity = "1"
    let note: String?
    var image: GiftImage?
    @ServerTimestamp var createdAt: Timestamp?
    var updatedAt: Timestamp? // Set it to nil in order for the value to be updated automatically by the server.
        
    init(uid: String, occasion: Occasion, name: String, senders: Set<Sender>?, quantity: String, note: String?, image: GiftImage? = nil) {
        self.uid = uid
        self.occasion = occasion
        self.name = name
        self.note = note
        self.quantity = quantity
        self.image = image
                
        if let senders = senders {
            if senders.count == 1 {
                if let firstSender = senders.first {
                    let sender = Sender(id: firstSender.id, firstName: firstSender.firstName, lastName: firstSender.lastName)
                    self.sender = sender
                }
            }
            self.senders = senders.map { $0.id }
        }
    }
    
    init(id: String, uid: String, occasion: Occasion, name: String, senders: Set<Sender>?, note: String?, quantity: String, image: GiftImage? = nil) {
        self.id = id
        self.uid = uid
        self.occasion = occasion
        self.name = name
        self.note = note
        self.quantity = quantity
        self.image = image
        
        if let senders = senders {
            if senders.count == 1 {
                if let firstSender = senders.first {
                    let sender = Sender(id: firstSender.id, firstName: firstSender.firstName, lastName: firstSender.lastName)
                    self.sender = sender
                }
            }
            self.senders = senders.map { $0.id }
        }
    }
    
    /*
    init(snapshot: DocumentSnapshot) {
        self.id = snapshot.get(DOCUMENT_GIFT_FIELDNAME_ID) as? String ?? ""
        self.uid = snapshot.get(DOCUMENT_GIFT_FIELDNAME_UID) as? String ?? ""
        self.occasionId = snapshot.get(DOCUMENT_GIFT_FIELDNAME_OCCASION_ID) as? String ?? ""
        self.occasionName = snapshot.get(DOCUMENT_GIFT_FIELDNAME_OCCASION_NAME) as? String ?? ""
        self.giftState = snapshot.get(DOCUMENT_GIFT_FIELDNAME_GIFT_STATE) as? String ?? ""
        self.name = snapshot.get(DOCUMENT_GIFT_FIELDNAME_NAME) as? String ?? ""
        
        
        if let recipientId = snapshot.get(DOCUMENT_GIFT_FIELDNAME_RECIPIENT_ID) as? String {
            let firstName = snapshot.get(DOCUMENT_GIFT_FIELDNAME_RECIPIENT_FIRST_NAME) as? String ?? ""
            let lastName = snapshot.get(DOCUMENT_GIFT_FIELDNAME_RECIPIENT_LAST_NAME) as? String
            let recipient = Recipient(id: recipientId, firstName: firstName, lastName: lastName)
            self.recipient = recipient
        }
        
        
        self.recipients = snapshot.get(DOCUMENT_GIFT_FIELDNAME_OCCASION_NAME) as? [String] ?? nil
        
        self.reaction = snapshot.get(DOCUMENT_GIFT_FIELDNAME_REACTION) as? String ?? ""
        
        
        
    }
 */
}

