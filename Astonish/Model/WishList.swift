//
//  WishList.swift
//  Astonish
//
//  Created by Øystein Günther on 02/02/2021.
//

import SwiftUI
import FirebaseFirestoreSwift
import FirebaseFirestore

struct WishList: Identifiable, Codable {
    @DocumentID var id: String? = UUID().uuidString
    var recipient: Recipient
    let uid: String
    let occasion: Occasion
    let name: String
    //@ServerTimestamp var createdAt: Timestamp?
    //var updatedAt: Timestamp? // Set it to nil in order for the value to be updated automatically by the server.
    //@ExplicitNull
    
    init(uid: String, recipient: Recipient, occasion: Occasion, name: String) {
        self.uid = uid
        self.recipient = recipient
        self.occasion = occasion
        self.name = name
    }
    
    init(id: String, uid: String, recipient: Recipient, occasion: Occasion, name: String) {
        self.id = id
        self.uid = uid
        self.recipient = recipient
        self.occasion = occasion
        self.name = name
    }
}

