//
//  OccasionGroup.swift
//  Astonish
//
//  Created by Øystein Günther on 28/01/2021.
//

import SwiftUI

struct OccasionGroup: Identifiable {
    let id: String
    let name: String
}
