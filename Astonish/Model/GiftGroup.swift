//
//  GiftGroup.swift
//  Astonish
//
//  Created by Øystein Günther on 20/01/2021.
//

import SwiftUI

struct GiftGroup: Identifiable {
    let id: String
    let name: String
}
