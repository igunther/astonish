//
//  User.swift
//  Astonish
//
//  Created by Øystein Günther on 23/12/2020.
//

struct User: Identifiable {
    let id: String
    let firstName: String
    let lastName: String
    let profileImageUrl: String
    let email: String
    
    init(dictionary: [String: Any]) {
        self.id = dictionary["uid"] as? String ?? ""
        self.firstName = dictionary["firstName"] as? String ?? ""
        self.lastName = dictionary["lastName"] as? String ?? ""
        self.profileImageUrl = dictionary["profileImageUrl"] as? String ?? ""
        self.email = dictionary["email"] as? String ?? ""
    }
}
