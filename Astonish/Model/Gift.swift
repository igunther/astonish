//
//  Gift.swift
//  Astonish
//
//  Created by Øystein Günther on 10/12/2020.
//

import SwiftUI
import FirebaseFirestoreSwift
import FirebaseFirestore

struct Gift: Identifiable, Codable {
    @DocumentID var id: String? = UUID().uuidString
    let uid: String
    let occasionId: String?
    let occasionName: String?
    let giftState: String
    let name: String
    @ExplicitNull var recipient: Recipient?
    let recipients: [String]?
    @ServerTimestamp var createdAt: Timestamp?
    var updatedAt: Timestamp? // Set it to nil in order for the value to be updated automatically by the server.
    let reaction: String
    var giftImageId: String?
    var giftImageUrl: String?
    
    init(uid: String, occasionId: String?, occasionName: String?, giftState: String, name: String, recipient: Recipient?, recipients: [String]?, reaction: String) {
        //self.id = UUID().uuidString
        self.uid = uid
        self.occasionId = occasionId
        self.occasionName = occasionName
        self.giftState = giftState
        self.name = name
        self.recipient = recipient
        self.recipients = recipients
        self.reaction = reaction
    }
    
    init(snapshot: DocumentSnapshot) {
        self.id = snapshot.get(DOCUMENT_GIFT_FIELDNAME_ID) as? String ?? ""
        self.uid = snapshot.get(DOCUMENT_GIFT_FIELDNAME_UID) as? String ?? ""
        self.occasionId = snapshot.get(DOCUMENT_GIFT_FIELDNAME_OCCASION_ID) as? String ?? ""
        self.occasionName = snapshot.get(DOCUMENT_GIFT_FIELDNAME_OCCASION_NAME) as? String ?? ""
        self.giftState = snapshot.get(DOCUMENT_GIFT_FIELDNAME_GIFT_STATE) as? String ?? ""
        self.name = snapshot.get(DOCUMENT_GIFT_FIELDNAME_NAME) as? String ?? ""
        
        
        if let recipientId = snapshot.get(DOCUMENT_GIFT_FIELDNAME_RECIPIENT_ID) as? String {
            let firstName = snapshot.get(DOCUMENT_GIFT_FIELDNAME_RECIPIENT_FIRST_NAME) as? String ?? ""
            let lastName = snapshot.get(DOCUMENT_GIFT_FIELDNAME_RECIPIENT_LAST_NAME) as? String
            let recipient = Recipient(id: recipientId, firstName: firstName, lastName: lastName)
            self.recipient = recipient
        }
        
        self.recipients = snapshot.get(DOCUMENT_GIFT_FIELDNAME_OCCASION_NAME) as? [String] ?? nil
        self.reaction = snapshot.get(DOCUMENT_GIFT_FIELDNAME_REACTION) as? String ?? ""
    }
}
