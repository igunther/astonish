//
//  Occasion.swift
//  Astonish
//
//  Created by Øystein Günther on 28/01/2021.
//

import SwiftUI
import Firebase

struct Occasion: Codable, Hashable, Identifiable {
    let id: String
    let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    init(dictionary: [String: Any]) {
        self.id = dictionary[DOCUMENT_OCCASION_FIELDNAME_ID] as? String ?? ""
        self.name = dictionary[DOCUMENT_OCCASION_FIELDNAME_NAME] as? String ?? ""
    }
    
//    init(snapshot: DocumentSnapshot) {
//        self.id = snapshot.get(DOCUMENT_RECIPIENT_FIELDNAME_ID) as? String ?? ""
//        self.firstName = snapshot.get(DOCUMENT_RECIPIENT_FIELDNAME_FIRSTNAME) as? String ?? ""
//        self.lastName = snapshot.get(DOCUMENT_RECIPIENT_FIELDNAME_LASTNAME) as? String ?? ""
//    }
}
