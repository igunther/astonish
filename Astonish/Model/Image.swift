//
//  Image.swift
//  Astonish
//
//  Created by Øystein Günther on 28/01/2021.
//

import SwiftUI
import Firebase

struct GiftImage: Codable, Hashable, Identifiable {
    let id: String
    let url: String
    
    init(id: String, url: String) {
        self.id = id
        self.url = url
    }
}
