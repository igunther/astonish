//
//  DocumentDates.swift
//  Astonish
//
//  Created by Øystein Günther on 12/01/2021.
//

import SwiftUI
import FirebaseFirestoreSwift
import FirebaseFirestore

struct DocumentDates: Codable {
    @ServerTimestamp var createdAt: Timestamp?
    @ServerTimestamp var updatedAt: Timestamp?  // Set it to nil in order for the value to be updated automatically by the server.
}
