//
//  PersonGroup.swift
//  Astonish
//
//  Created by Øystein Günther on 02/01/2021.
//

import SwiftUI

struct PersonGroup: Identifiable {
    let id: String
    let name: String
}
