//
//  Person.swift
//  Astonish
//
//  Created by Øystein Günther on 31/12/2020.
//

import SwiftUI

struct Person: Identifiable, Hashable {
    let id: String
    let firstName: String
    let lastName: String?
    let profileImageUrl: String?
    let profileImageId: String
    let email: String
    let group: String?
    
    var fullName: String {
        get {
            let firstName = self.firstName
            let lastName = self.lastName == nil ? "" : self.lastName!
                    
            if firstName != "" && lastName != "" {
                return firstName + " " + lastName
            } else if firstName == "" {
                return lastName
            }
            return firstName
        }
    }
    
    init(dictionary: [String: Any]) {
        self.id = dictionary[DOCUMENT_PERSON_FIELDNAME_ID] as? String ?? ""
        self.firstName = dictionary[DOCUMENT_PERSON_FIELDNAME_FIRST_NAME] as? String ?? ""
        self.lastName = dictionary[DOCUMENT_PERSON_FIELDNAME_LAST_NAME] as? String ?? ""
        self.profileImageUrl = dictionary[DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_URL] as? String ?? ""
        self.profileImageId = dictionary[DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_ID] as? String ?? ""
        self.email = dictionary[DOCUMENT_PERSON_FIELDNAME_EMAIL] as? String ?? ""
        self.group = dictionary[DOCUMENT_PERSON_FIELDNAME_GROUP] as? String ?? ""
    }
}


