//
//  GiftState.swift
//  Astonish
//
//  Created by Øystein Günther on 18/01/2021.
//

enum GiftState: String, CaseIterable {
    case idea
    case toBuy
    case ordered
    case toWrap
    case wrapped
    case gifted
    
    var systemImage: String  {
        switch self {
        case .idea:
            return "lightbulb"
        case .toBuy:
            return "cart"
        case .ordered:
            return "shippingbox"
        case .toWrap:
            return "gift"
        case .wrapped:
            return "gift.fill"
        case .gifted:
            return "checkmark.shield"
        }
    }
}
