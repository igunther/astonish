//
//  StorageImageHelper.swift
//  Astonish
//
//  Created by Øystein Günther on 01/01/2021.
//

import Firebase
import SwiftUI

enum StorageFileType: String {
    case image
}

enum StorageContext: String {
    case profile
    case person
    case gift
    case toMe
}

struct StorageImageHelper {
    
    static func uploadImage(image: UIImage, fileType: StorageFileType, context: StorageContext, userId: String, completion: @escaping (Bool, String, String) -> Void) {
                
        guard let imageData = image.jpegData(compressionQuality: 0.3) else {
            completion(false, "", "")
            return
        }
                        
        let fileName = userId + "/" + fileType.rawValue + "/" + context.rawValue + "/" + UUID().uuidString
        let storageRef = Storage.storage().reference().child(fileName)
            
        storageRef.putData(imageData, metadata: nil) {_, error in
            if let error = error {
                swiftyBeaverLog.error("Failed to upload image, error: \(error.localizedDescription)")
                completion(false, "", "")
                return
            }
            
            storageRef.downloadURL { url, _ in
                guard let imageUrl = url?.absoluteString else {
                    completion(false, "", "")
                    return
                }
                
                completion(true, imageUrl, fileName)
            }
        }
    }
    
    static func deleteImage(documentId: String, completion: @escaping (Bool) -> Void) {
        let storageRef = Storage.storage().reference().child(documentId)
        storageRef.delete { error in
            if let error = error {
                swiftyBeaverLog.error("Failed to delete document: \(error.localizedDescription)")
                completion(false)
            } else {
                completion(true)
            }
        }
    }
}
