//
//  WishesViewModel.swift
//  Astonish
//
//  Created by Øystein Günther on 04/02/2021.
//

import SwiftUI
import FirebaseFirestoreSwift
import Firebase

class WishesViewModel: ObservableObject {
    
    // MARK: - Public properties
    
    @Published var wishes = [Wish]()
        
    // MARK: - Functions
    
    func get(wishListId: String?) {
        guard let wishListId = wishListId else { return }
        COLLECTION_WISHES.whereField(DOCUMENT_WISHES_FIELDNAME_WISH_LIST_ID, in: [wishListId]).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                return
            }
            
            self.wishes = documents.compactMap { querySnapshot -> Wish? in
                return try? querySnapshot.data(as: Wish.self)
            }
            
            swiftyBeaverLog.debug("wishes.count = \(self.wishes.count)")
        }
    }
    
    func add(wish: Wish) {
        do {
            let _ = try COLLECTION_WISHES.addDocument(from: wish)
        } catch {
            swiftyBeaverLog.error(error)
        }
    }
    
    func update(_ wish: Wish) {
        guard let documentId = wish.id else { return }
        
        do {
            let _ = try COLLECTION_WISHES.document(documentId).setData(from: wish)
        } catch {
            swiftyBeaverLog.error(error)
        }
    }
}

