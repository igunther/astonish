//
//  ToMeViewModel.swift
//  Astonish
//
//  Created by Øystein Günther on 27/01/2021.
//

import SwiftUI
import FirebaseFirestoreSwift
import Firebase

class ToMeViewModel: ObservableObject {
    
    // MARK: - Public properties
    
    @Published var giftsToMe = [ToMe]()
    @Published var occasions = [OccasionGroup]()
    
    // MARK: - Functions
    
    func get(uid: String?) {
        guard let uid = uid else { return }
        COLLECTION_TO_ME.whereField(DOCUMENT_TO_ME_FIELDNAME_UID, in: [uid]).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                swiftyBeaverLog.verbose("No gifts to me for this uid: \(uid)")
                return
            }
            
            self.giftsToMe = documents.compactMap { querySnapshot -> ToMe? in
                return try? querySnapshot.data(as: ToMe.self)
            }
            
            swiftyBeaverLog.debug("giftsToMe.count = \(self.giftsToMe.count)")
            
            self.initOccasions()
        }
    }
    
    func add(toMe: ToMe, giftImage: UIImage?) {
            
        if let giftImage = giftImage {
            StorageImageHelper.uploadImage(image: giftImage, fileType: .image, context: .toMe, userId: toMe.uid) { (success, giftImageUrl, giftImageId) in
                
                var toMeWithImage: ToMe = toMe
                toMeWithImage.image = GiftImage(id: giftImageId, url: giftImageUrl)
                                
                do {
                    let _ = try COLLECTION_TO_ME.addDocument(from: toMeWithImage)
                } catch {
                    swiftyBeaverLog.error(error)
                }
            }
        } else {
            do {
                let _ = try COLLECTION_TO_ME.addDocument(from: toMe)
            } catch {
                swiftyBeaverLog.error(error)
            }
        }
    }
    
    func update(_ toMe: ToMe, giftImage: UIImage?) {
        guard let documentId = toMe.id else { return }
        
        if let giftImage = giftImage {
            StorageImageHelper.uploadImage(image: giftImage, fileType: .image, context: .toMe, userId: toMe.uid) { (success, giftImageUrl, giftImageId) in
                
                var toMeWithImage: ToMe = toMe
                toMeWithImage.image = GiftImage(id: giftImageId, url: giftImageUrl)
                
                do {
                    let _ = try COLLECTION_TO_ME.document(documentId).setData(from: toMeWithImage)
                } catch {
                    swiftyBeaverLog.error(error)
                }
            }
        } else {
            do {
                let _ = try COLLECTION_TO_ME.document(documentId).setData(from: toMe)
            } catch {
                swiftyBeaverLog.error(error)
            }
        }
    }
    
    private func initOccasions() {
        let allOccasions = giftsToMe.map { $0.occasion.name }
        let distinctOccasions = allOccasions.removingDuplicates()
        occasions.removeAll()
        distinctOccasions.forEach { name in
            occasions.append(OccasionGroup(id: UUID().uuidString, name: name))
        }
    }
    
    func toMeIn(_ occasionName: String) -> [ToMe] {
        var toMeInOccasion = [ToMe]()
        giftsToMe.forEach { toMe in
            if toMe.occasion.name == occasionName {
                toMeInOccasion.append(toMe)
            }
        }
        return toMeInOccasion
    }
}
