//
//  GiftViewModel.swift
//  Astonish
//
//  Created by Øystein Günther on 29/12/2020.
//

import SwiftUI
import FirebaseFirestoreSwift
import Firebase

class GiftViewModel: ObservableObject {
    
    // MARK: - Public properties
    
    @Published var gifts = [Gift]()
    @Published var giftGroups: [GiftGroup] =
            [GiftGroup(id: GiftState.idea.rawValue, name: GiftState.idea.rawValue),
             GiftGroup(id: GiftState.toBuy.rawValue, name: GiftState.toBuy.rawValue),
             GiftGroup(id: GiftState.ordered.rawValue, name: GiftState.ordered.rawValue),
             GiftGroup(id: GiftState.toWrap.rawValue, name: GiftState.toWrap.rawValue),
             GiftGroup(id: GiftState.wrapped.rawValue, name: GiftState.wrapped.rawValue),
             GiftGroup(id: GiftState.gifted.rawValue, name: GiftState.gifted.rawValue)]
        
    // MARK: - Internal properties
    
    // MARK: - Constructores
    
    // MARK: - Functions
    
    func get(uid: String?) {
        guard let uid = uid else { return }
        COLLECTION_GIFTS.whereField(DOCUMENT_GIFT_FIELDNAME_UID, in: [uid]).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                swiftyBeaverLog.verbose("No gifts for this uid: \(uid)")
                return
            }
            
            self.gifts = documents.compactMap { querySnapshot -> Gift? in
                return try? querySnapshot.data(as: Gift.self)
            }
        }
    }
    
    func get(occasionId: String) {
        COLLECTION_GIFTS.whereField(DOCUMENT_GIFT_FIELDNAME_OCCASION_ID, in: [occasionId]).addSnapshotListener { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                swiftyBeaverLog.verbose("No gifts for this occasionId.")
                return
            }
            
            self.gifts = documents.compactMap { querySnapshot -> Gift? in
                return try? querySnapshot.data(as: Gift.self)
            }
        }
    }
    
    func get(recipientId: String) {
        COLLECTION_GIFTS.whereField(DOCUMENT_GIFT_FIELDNAME_RECIPIENTS, arrayContains: recipientId).addSnapshotListener { snapshot, error in
            if let error = error {
                swiftyBeaverLog.error("No gifts for this uid. error: \(error)")
                return
            }
            
            guard let documents = snapshot?.documents else {
                swiftyBeaverLog.verbose("No gifts for this uid.")
                return
            }
            
            self.gifts = documents.compactMap { querySnapshot -> Gift? in
                return try? querySnapshot.data(as: Gift.self)
            }
        }
    }
    
    func add(gift: Gift, giftImage: UIImage?) {
            
        if let giftImage = giftImage {
            StorageImageHelper.uploadImage(image: giftImage, fileType: .image, context: .gift, userId: gift.uid) { (success, giftImageUrl, giftImageId) in
                
                var giftWithImage: Gift = gift
                giftWithImage.giftImageId = giftImageId
                giftWithImage.giftImageUrl = giftImageUrl
                
                do {
                    let _ = try COLLECTION_GIFTS.addDocument(from: giftWithImage)
                } catch {
                    swiftyBeaverLog.error(error)
                }
            }
        } else {
            do {
                let _ = try COLLECTION_GIFTS.addDocument(from: gift)
            } catch {
                swiftyBeaverLog.error(error)
            }
        }
    }
    
    
    func update(firstName: String, lastName: String?, recipientId: String) {
        COLLECTION_GIFTS.whereField(DOCUMENT_GIFT_FIELDNAME_RECIPIENTS, arrayContains: recipientId).getDocuments { (querySnapshot, error) in
            
            swiftyBeaverLog.debug("We have gifts for: \(recipientId)")
            
            guard let _ = querySnapshot?.documents else { return }
            guard let querySnapshot = querySnapshot else { return }
            
            // Get the new write batch
            //let batch = FIRESTORE.batch()
            
            for doc in querySnapshot.documents {
                let gift = Gift(snapshot: doc)
                
                if gift.recipient?.id == recipientId {
                    let data = [DOCUMENT_GIFT_FIELDNAME_RECIPIENT_FIRST_NAME: firstName,
                                DOCUMENT_GIFT_FIELDNAME_RECIPIENT_LAST_NAME: lastName ?? ""]
                    
                    //batch.setData(data, forDocument: doc, merge: true)
                    
                    COLLECTION_GIFTS.document(doc.documentID).updateData(data) { _ in
                        swiftyBeaverLog.debug("Successfully updated gift")
                    }
                }
            }
        }
    }
    
    func update(uid: String, id: String?, name: String, occasionId: String?, occasionName: String?, giftState: String, reaction: String, recipients: Set<Recipient>?, giftImage: UIImage?, giftImageId: String?) {
        guard let id = id else { return }
                
        func update(_ giftImageUrl: String? = nil, _ giftImageId: String? = nil) {
            var recipientsIds = [String]()
            var recipientId = ""
            var recipientFirstName = ""
            var recipientLastName = ""
            
            if let recipients = recipients {
                if recipients.count == 1 {
                    if let firstRecipient = recipients.first {
                        recipientId = firstRecipient.id
                        recipientFirstName = firstRecipient.firstName
                        recipientLastName = firstRecipient.lastName ?? ""
                    }
                }
                recipientsIds = recipients.map { $0.id }
            }
            
            var data = [DOCUMENT_GIFT_FIELDNAME_NAME: name,
                        DOCUMENT_GIFT_FIELDNAME_OCCASION_ID: occasionId ?? "",
                        DOCUMENT_GIFT_FIELDNAME_OCCASION_NAME: occasionName ?? "",
                        DOCUMENT_GIFT_FIELDNAME_GIFT_STATE: giftState,
                        DOCUMENT_GIFT_FIELDNAME_REACTION: reaction,
                        DOCUMENT_GIFT_FIELDNAME_RECIPIENT_ID: recipientId,
                        DOCUMENT_GIFT_FIELDNAME_RECIPIENT_FIRST_NAME: recipientFirstName,
                        DOCUMENT_GIFT_FIELDNAME_RECIPIENT_LAST_NAME: recipientLastName,
                        DOCUMENT_GIFT_FIELDNAME_RECIPIENTS: recipientsIds,
                        DOCUMENT_GIFT_FIELDNAME_UPDATED_AT: FieldValue.serverTimestamp()
            ] as [String : Any]
            
            // If we have changed to another image.
            if let _ = giftImage {
                data[DOCUMENT_GIFT_FIELDNAME_GIFT_IMAGE_URL] = giftImageUrl
                data[DOCUMENT_GIFT_FIELDNAME_GIFT_IMAGE_ID] = giftImageId
            }
            
            COLLECTION_GIFTS.document(id).updateData(data) { _ in
                swiftyBeaverLog.debug("Successfully updated gift")
            }
        }
        
        if let giftImage = giftImage {
            if let giftImageId = giftImageId {
                StorageImageHelper.deleteImage(documentId: giftImageId) { success in
                    //
                }
            }
            
            StorageImageHelper.uploadImage(image: giftImage, fileType: .image, context: .gift, userId: uid) { (success, giftImageUrl, giftImageId) in
                update(giftImageUrl, giftImageId)
            }
        } else {
            update()
        }
    }
    
    // Not tested, but I should concider using this approach!
    func update(gift: Gift, giftImage: UIImage?) {
        guard let documentId = gift.id else { return }
        
        if let giftImage = giftImage {
            StorageImageHelper.uploadImage(image: giftImage, fileType: .image, context: .gift, userId: gift.uid) { (success, giftImageUrl, giftImageId) in
                
                var giftWithImage: Gift = gift
                giftWithImage.giftImageId = giftImageId
                giftWithImage.giftImageUrl = giftImageUrl
                
                do {
                    let _ = try COLLECTION_GIFTS.document(documentId).setData(from: giftWithImage)
                } catch {
                    swiftyBeaverLog.error(error)
                }
            }
        } else {
            do {
                let _ = try COLLECTION_GIFTS.document(documentId).setData(from: gift)
            } catch {
                swiftyBeaverLog.error(error)
            }
        }
        
        
//        do {
//            try COLLECTION_GIFTS.document(documentId).setData(from: gift)
//        } catch {
//            swiftyBeaverLog.error("error: \(error)")
//        }
    }
    
    
    func giftsIn(_ group: String) -> [Gift] {
        var giftsInGroup = [Gift]()
        gifts.forEach { gift in
            if gift.giftState == group {
                giftsInGroup.append(gift)
            }
        }
        return giftsInGroup
    }
    
    
    /*
    func createGift(uid: String, name: String, occasionId: String, giftImage: UIImage?, recipients: Set<Person>?) {
        
        func setGiftData(_ giftImageUrl: String? = nil, _ giftImageId: String? = nil) {
            let id = UUID().uuidString
            
            let data = [DOCUMENT_GIFT_FIELDNAME_ID: id,
                        DOCUMENT_GIFT_FIELDNAME_UID: uid,
                        DOCUMENT_GIFT_FIELDNAME_NAME: name,
                        DOCUMENT_GIFT_FIELDNAME_GIFT_IMAGE_URL: giftImageUrl,
                        DOCUMENT_GIFT_FIELDNAME_GIFT_IMAGE_ID: giftImageId,
                        DOCUMENT_GIFT_FIELDNAME_OCCASION_ID: occasionId
            ]
            
            let giftRef = COLLECTION_GIFTS.document(id)
            
            giftRef.setData(data as [String: Any]) { _ in
                swiftyBeaverLog.debug("Successfully uploaded gift data")
                
                if let recipients = recipients {
                    let recipientsIds = recipients.map { $0.id }
                    giftRef.updateData( [DOCUMENT_GIFT_FIELDNAME_RECIPIENTS: recipientsIds]) { _ in
                        swiftyBeaverLog.debug("Successfully uploaded gift recipients")
                    }
                }
                
                /*
                if let recipients = recipients {
                    
                    var recipientsIds = [String]()
                    
                    giftRef.setData( [
                        "recipients": ""
                    ])
                    
                    for person in recipients {
                        recipientsIds.append(person.id)
                        
                        // Add the person.id to the end of the array.
                        // Will be added only if it does not exist.
                        giftRef.updateData( [
                            "recipients": FieldValue.arrayUnion([person.id])
                        ])
                        
                        
                        
                    }
                }
                */
            }
        }
        
        if let giftImage = giftImage {
            StorageImageHelper.uploadImage(image: giftImage, fileType: .image, context: .gift, userId: uid) { (success, giftImageUrl, giftImageId) in
                setGiftData(giftImageUrl, giftImageId)
            }
        } else {
            setGiftData()
        }
    }
 */
}
