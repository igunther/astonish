//
//  AuthViewModel.swift
//  Astonish
//
//  Created by Øystein Günther on 22/12/2020.
//

import SwiftUI
import Firebase

class AuthViewModel: ObservableObject {
    @Published var userSession: FirebaseAuth.User?
    @Published var isAuthenticating = false
    @Published var error: Error?
    //@Published var user: User?
    
    init() {
        userSession = Auth.auth().currentUser // If it is nil, the user is not logged in.
        fetchUser()
    }
    
    func login(withEmail email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            if let error = error {
                swiftyBeaverLog.error("Failed to log in, Error: \(error.localizedDescription)")
                return
            }
            
            self.userSession = result?.user
            
            swiftyBeaverLog.verbose("Successfully signed in")
        }
    }
    
    func registerUser(email: String, password: String, firstName: String, lastName: String, profileImage: UIImage) {
        
        guard let imageData = profileImage.jpegData(compressionQuality: 0.3) else { return }
        let fileName = NSUUID().uuidString
        let storageRef = Storage.storage().reference().child(fileName)
        
        storageRef.putData(imageData, metadata: nil) {_, error in
            if let error = error {
                swiftyBeaverLog.error("Failed to upload image, error: \(error.localizedDescription)")
                return
            }
            
            storageRef.downloadURL { url, _ in
                guard let profileImageUrl = url?.absoluteString else { return }
                
                Auth.auth().createUser(withEmail: email, password: password) { result, error in
                    if let error = error {
                        swiftyBeaverLog.error("Error: \(error.localizedDescription)")
                        return
                    }
                    
                    guard let user = result?.user else { return }
                   
                    let data = ["email": email,
                                "firstName": firstName,
                                "lastName": lastName,
                                "profileImageUrl": profileImageUrl,
                                "uid": user.uid]
                    
                    Firestore.firestore().collection("users").document(user.uid).setData(data) { _ in
                        swiftyBeaverLog.debug("Successfully uploaded user data")
                        self.userSession = user
                    }
                    
                }
            }
            swiftyBeaverLog.debug("Successfully signed up user")
        }
    }
    
    func signOut() {
        userSession = nil
        try? Auth.auth().signOut()
    }
    
    func fetchUser() {
        guard let uid = userSession?.uid else { return }
        Firestore.firestore().collection("users").document(uid).getDocument { snapshot, _ in
            guard let data = snapshot?.data() else { return }
            let user = User(dictionary: data)
            swiftyBeaverLog.debug("User is \(user.firstName)")
        }
    }
}
