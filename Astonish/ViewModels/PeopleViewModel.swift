//
//  PeopleViewModel.swift
//  Astonish
//
//  Created by Øystein Günther on 31/12/2020.
//

import SwiftUI
import FirebaseFirestore
import Firebase

class PeopleViewModel: ObservableObject {
    
    // MARK: - Public Properties
    
    @Published var people = [Person]()
    @Published var peopleArray = [Person]()
    @Published var personGroups = [PersonGroup]()
    @Published var recipients = [Recipient]()
    @Published var senders = [Sender]()
    
    // MARK: - Internal Properties
    
    // MARK: - Constructors
    
    // MARK: - Functions
    
    func getPeople(idArray: [String]) {
                
        COLLECTION_PEOPLE.whereField(DOCUMENT_PERSON_FIELDNAME_UID, in: idArray).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                return
            }
            
            self.peopleArray = documents.map { querySnapshot -> Person in
                let data = querySnapshot.data()
                let personData = [DOCUMENT_PERSON_FIELDNAME_ID: data[DOCUMENT_PERSON_FIELDNAME_ID] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_FIRST_NAME: data[DOCUMENT_PERSON_FIELDNAME_FIRST_NAME] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_LAST_NAME: data[DOCUMENT_PERSON_FIELDNAME_LAST_NAME] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_GROUP: data[DOCUMENT_PERSON_FIELDNAME_GROUP] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_URL: data[DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_URL] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_ID: data[DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_ID] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_EMAIL: data[DOCUMENT_PERSON_FIELDNAME_EMAIL] as? String ?? ""
                ]
                
                return Person(dictionary: personData)
            }
            self.initGroups()
        }
    }
    
    func fetchPeople(uid: String?) {
        guard let uid = uid else { return }
        
        COLLECTION_PEOPLE.whereField(DOCUMENT_PERSON_FIELDNAME_UID, in: [uid]).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                swiftyBeaverLog.verbose("No people for this uid: \(uid)")
                return
            }
            
            self.people = documents.map { querySnapshot -> Person in
                let data = querySnapshot.data()
                let giftData = [DOCUMENT_PERSON_FIELDNAME_ID: data[DOCUMENT_PERSON_FIELDNAME_ID] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_FIRST_NAME: data[DOCUMENT_PERSON_FIELDNAME_FIRST_NAME] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_LAST_NAME: data[DOCUMENT_PERSON_FIELDNAME_LAST_NAME] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_GROUP: data[DOCUMENT_PERSON_FIELDNAME_GROUP] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_URL: data[DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_URL] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_ID: data[DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_ID] as? String ?? "",
                                DOCUMENT_PERSON_FIELDNAME_EMAIL: data[DOCUMENT_PERSON_FIELDNAME_EMAIL] as? String ?? ""
                ]
                
                swiftyBeaverLog.debug("Returning people from fetchPeople")
                
                return Person(dictionary: giftData)
            }
            self.initGroups()
        }
    }
    
    func getRecipients(uid: String?) {
        guard let uid = uid else { return }
        
        COLLECTION_PEOPLE.whereField(DOCUMENT_PERSON_FIELDNAME_UID, in: [uid]).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                swiftyBeaverLog.verbose("No people for this uid: \(uid)")
                return
            }
            
            self.recipients = documents.map { querySnapshot -> Recipient in
                let data = querySnapshot.data()
                
                return Recipient(id: data[DOCUMENT_PERSON_FIELDNAME_ID] as? String ?? "",
                                 firstName: data[DOCUMENT_PERSON_FIELDNAME_FIRST_NAME] as? String ?? "",
                                 lastName: data[DOCUMENT_PERSON_FIELDNAME_LAST_NAME] as? String ?? "")
            }
            self.initGroups()
        }
    }
    
    func getRecipients(recipientIds: [String], completion: @escaping ([Recipient]?) -> Void) {
        COLLECTION_PEOPLE.whereField(DOCUMENT_PERSON_FIELDNAME_ID, in: recipientIds).addSnapshotListener { (querySnapshot, error) in
  
            guard let _ = querySnapshot?.documents else { return }
            guard let querySnapshot = querySnapshot else { return }
            
            var recipients = [Recipient]()
            
            for doc in querySnapshot.documents {
                let recipient = Recipient(snapshot: doc)
                recipients.append(recipient)
            }
            completion(recipients)
        }
    }
    
    func getSenders(uid: String?) {
        guard let uid = uid else { return }
        
        COLLECTION_PEOPLE.whereField(DOCUMENT_PERSON_FIELDNAME_UID, in: [uid]).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                swiftyBeaverLog.verbose("No people for this uid: \(uid)")
                return
            }
            
            self.senders = documents.map { querySnapshot -> Sender in
                let data = querySnapshot.data()
                
                return Sender(id: data[DOCUMENT_PERSON_FIELDNAME_ID] as? String ?? "",
                                 firstName: data[DOCUMENT_PERSON_FIELDNAME_FIRST_NAME] as? String ?? "",
                                 lastName: data[DOCUMENT_PERSON_FIELDNAME_LAST_NAME] as? String ?? "")
            }
        }
    }
    
    func getSenders(senderIds: [String], completion: @escaping ([Sender]?) -> Void) {
        COLLECTION_PEOPLE.whereField(DOCUMENT_PERSON_FIELDNAME_ID, in: senderIds).addSnapshotListener { (querySnapshot, error) in
  
            guard let _ = querySnapshot?.documents else { return }
            guard let querySnapshot = querySnapshot else { return }
            
            var senders = [Sender]()
            
            for doc in querySnapshot.documents {
                let sender = Sender(snapshot: doc)
                senders.append(sender)
            }
            completion(senders)
        }
    }
        
    func createPerson(uid: String, firstName: String, lastName: String, group: String, image: UIImage?, email: String) {
        
        func setPersonData(_ profileImageUrl: String? = nil, _ profileImageId: String? = nil) {
            let id = UUID().uuidString
            let data = [DOCUMENT_PERSON_FIELDNAME_ID: id,
                        DOCUMENT_PERSON_FIELDNAME_UID: uid,
                        DOCUMENT_PERSON_FIELDNAME_FIRST_NAME: firstName,
                        DOCUMENT_PERSON_FIELDNAME_LAST_NAME: lastName,
                        DOCUMENT_PERSON_FIELDNAME_GROUP: group,
                        DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_URL: profileImageUrl,
                        DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_ID: profileImageId,
                        DOCUMENT_PERSON_FIELDNAME_EMAIL: email]
            COLLECTION_PEOPLE.document(id).setData(data as [String : Any]) { _ in
                swiftyBeaverLog.debug("Successfully uploaded person data")
            }
        }
        
        if let image = image {
            StorageImageHelper.uploadImage(image: image, fileType: .image, context: .person, userId: uid) { (success, profileImageUrl, profileImageId) in
                setPersonData(profileImageUrl, profileImageId)
            }
        } else {
            setPersonData()
        }
    }
        
    func updatePerson(uid: String, id: String, firstName: String, lastName: String, group: String, email: String, image: UIImage?, profileImageId: String?) {
        
        func updatePersonData(_ profileImageUrl: String? = nil, _ profileImageId: String? = nil) {
            var data = [DOCUMENT_PERSON_FIELDNAME_FIRST_NAME: firstName,
                        DOCUMENT_PERSON_FIELDNAME_LAST_NAME: lastName,
                        DOCUMENT_PERSON_FIELDNAME_GROUP: group,
                        DOCUMENT_PERSON_FIELDNAME_EMAIL: email]
            
            // If we have changed to another image.
            if let _ = image {
                data[DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_URL] = profileImageUrl
                data[DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_ID] = profileImageId
            }
                        
            COLLECTION_PEOPLE.document(id)
                .updateData(data as [AnyHashable : Any]) { _ in
                    let giftViewModel = GiftViewModel()
                    giftViewModel.update(firstName: firstName, lastName: lastName, recipientId: id)
                }
        }
        
        if let image = image {
            if let profileImageId = profileImageId {
                // Delete the old image from the database.
                StorageImageHelper.deleteImage(documentId: profileImageId) { success in
                    //
                }
            }
            
            StorageImageHelper.uploadImage(image: image, fileType: .image, context: .person, userId: uid) { (success, profileImageUrl, profileImageId) in
                updatePersonData(profileImageUrl, profileImageId)
            }
            
        } else {
            updatePersonData()
        }
    }
    
    func removeProfileImageFromPerson(id: String) {
        let data = [
            DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_URL: "",
            DOCUMENT_PERSON_FIELDNAME_PROFILE_IMAGE_ID: ""
        ]
        COLLECTION_PEOPLE.document(id)
            .updateData(data as [AnyHashable : Any])
    }
    
    private func initGroups() {
        let allGroups = people.map { $0.group }
        let distinctGroups = allGroups.removingDuplicates()
        personGroups.removeAll()
        swiftyBeaverLog.debug("initGroups")
        distinctGroups.forEach { group in
            //personGroups.append(PersonGroup(id: UUID().uuidString, name: group == "" ? "No Group" : group))
            if let group = group {
                personGroups.append(PersonGroup(id: UUID().uuidString, name: group ))
            }
        }
    }
    
    func peopleIn(_ group: String) -> [Person] {
        var peopleInGroup = [Person]()
        people.forEach { person in
            if person.group == group {
                peopleInGroup.append(person)
            }
        }
        return peopleInGroup
    }
}
