//
//  OccasionViewModel.swift
//  Astonish
//
//  Created by Øystein Günther on 25/12/2020.
//

import SwiftUI
import FirebaseFirestore

class OccasionViewModel: ObservableObject {
    
    // MARK: - Public properties
    
    //@Published var userSession: FirebaseAuth.User?
    @Published var occasions = [Occasion]()
    @Published var gifts = [Gift]()
    @EnvironmentObject var authViewModel: AuthViewModel
 
    // MARK: - Internal properties
    
    
    // MARK: - Constructors
    
    init() {
        
    }
    
    func get(uid: String?) {
/*
         COLLECTION_OCCASIONS.getDocuments { snapshot, _ in
            guard let documents = snapshot?.documents else { return }
            self.occasions = documents.map({ Occasion(dictionary: $0.data()) })
            
//            documents.forEach { document in
//                let occasion = Occasion(dictionary: document.data())
//
//                swiftyBeaverLog.debug("Reading occasion.name: \(occasion.name)")
//                
//                self.occasions.append(occasion)
//            }
            
        }
*/
        guard let uid = uid else { return }
        
        swiftyBeaverLog.debug("uid: \(uid)")
        
        COLLECTION_OCCASIONS.whereField(DOCUMENT_OCCASION_FIELDNAME_UID, in: [uid]).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                swiftyBeaverLog.error("No documents.")
                return
            }
            
            swiftyBeaverLog.debug("We have documents")
            
            self.occasions = documents.map { queryDocumentSnapShot -> Occasion in
                let data = queryDocumentSnapShot.data()
                
                let occasionData = [DOCUMENT_OCCASION_FIELDNAME_ID: data[DOCUMENT_OCCASION_FIELDNAME_ID] as? String ?? "",
                                 DOCUMENT_OCCASION_FIELDNAME_NAME: data[DOCUMENT_OCCASION_FIELDNAME_NAME] as? String ?? ""]
                
                swiftyBeaverLog.debug("Returning Occasion()")
                
                return Occasion(dictionary: occasionData)
            }
        }
    }
    
    func createOccasion(uid: String?, name: String) {
        let id = UUID().uuidString
        let data = [DOCUMENT_OCCASION_FIELDNAME_ID: id,
                    DOCUMENT_OCCASION_FIELDNAME_UID: uid,
                    DOCUMENT_OCCASION_FIELDNAME_NAME: name]
        COLLECTION_OCCASIONS.document(id).setData(data as [String : Any]) { _ in
            swiftyBeaverLog.debug("Successfully uploaded occasion data")
        }
        
    }
    
//    func fetchGifts(occasionId: String) {
//        FIRESTORE.collection("occasions").document(occasionId).collection("gifts").addSnapshotListener { (querySnapshot, error) in
//            guard let documents = querySnapshot?.documents else {
//                swiftyBeaverLog.error("No documents.")
//                return
//            }
//            
//            self.gifts = documents.map { queryDocumentSnapShot -> Gift in
//                let data = queryDocumentSnapShot.data()
//                
//                let giftData = [DOCUMENT_OCCASION_FIELDNAME_NAME: data[DOCUMENT_OCCASION_FIELDNAME_NAME] as? String ?? ""]
//                
//                swiftyBeaverLog.debug("We have gifts to return.")
//                
//                return Gift(dictionary: giftData)
//            }
//        }
//    }
}
