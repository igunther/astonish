//
//  WishListViewModel.swift
//  Astonish
//
//  Created by Øystein Günther on 01/02/2021.
//

import SwiftUI
import FirebaseFirestoreSwift
import Firebase

class WishListViewModel: ObservableObject {
    
    // MARK: - Public properties
    
    @Published var wishLists = [WishList]()
        
    // MARK: - Functions
    
    func get(uid: String?) {
        guard let uid = uid else { return }
        COLLECTION_WISH_LISTS.whereField(DOCUMENT_WISH_LIST_FIELDNAME_UID, in: [uid]).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                return
            }
            
            self.wishLists = documents.compactMap { querySnapshot -> WishList? in
                return try? querySnapshot.data(as: WishList.self)
            }
            
            swiftyBeaverLog.debug("uid: \(uid)")
            swiftyBeaverLog.debug("wishLists.count = \(self.wishLists.count)")
        }
    }
    
    func add(wishList: WishList) {
        do {
            let _ = try COLLECTION_WISH_LISTS.addDocument(from: wishList)
        } catch {
            swiftyBeaverLog.error(error)
        }
    }
    
    func update(_ wishList: WishList) {
        guard let documentId = wishList.id else { return }
        
        do {
            let _ = try COLLECTION_WISH_LISTS.document(documentId).setData(from: wishList)
        } catch {
            swiftyBeaverLog.error(error)
        }
    }
}

