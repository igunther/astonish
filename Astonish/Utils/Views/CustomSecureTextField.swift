//
//  CustomSecureTextField.swift
//  Astonish
//
//  Created by Øystein Günther on 21/12/2020.
//

import SwiftUI

struct CustomSecureTextField: View {
    // MARK: - Properties
    
    @Binding var text: String
    let placeholder: Text
    
    // MARK: - Body
    
    var body: some View {
                
        ZStack(alignment: .leading) {
            if text.isEmpty {
                placeholder
                    .foregroundColor(Color(.init(white: 1, alpha: 0.87)))
                    .padding(.leading, 40)
            }
            
            HStack(spacing: 16) {
                Image(systemName: "lock")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 20, height: 20)
                
                SecureField("", text: $text)
                
            } //: HStack
            
        } //: ZStack
    }
}

struct CustomSecureTextField_Previews: PreviewProvider {
    static var previews: some View {
        CustomSecureTextField(text: .constant(""), placeholder: Text("Password"))
    }
}
