//
//  NumbersOnly.swift
//  Astonish
//
//  Created by Øystein Günther on 25/01/2021.
//

import SwiftUI

class NumbersOnly: ObservableObject {
    @Published var value = "" {
        didSet {
            let filtered = value.filter { $0.isNumber }
            
            if value != filtered {
                value = filtered
            }
        }
    }
}
