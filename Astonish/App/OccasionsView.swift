//
//  OccasionsView.swift
//  Astonish
//
//  Created by Øystein Günther on 17/12/2020.
//

import SwiftUI

enum ActiveSheet: Identifiable {
    case first, second
    
    var id: Int {
        hashValue
    }
}

struct OccasionsView: View {
    
    // MARK: - Properties
    
    @State private var activeSheet: ActiveSheet?
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var occasionViewModel = OccasionViewModel()
    
    // MARK: - Body
    
    var body: some View {
        
        NavigationView {
            
            ZStack(alignment: .bottomTrailing) {
                VStack(alignment: .leading) {
                    
                    List(occasionViewModel.occasions) { occasion in
                        NavigationLink(destination: OccasionGifts(occasion: occasion)) {
                            OccasionCell(occasion: occasion)
                        }
                    }
                    .listStyle(InsetGroupedListStyle())
                } //: VStack
                
                RoundActionButton(systemImage: "calendar.badge.plus") {
                    activeSheet = .first
                    //authViewModel.signOut() // Testing
                }
                
            } //: ZStack
            
            .onAppear() {
                self.occasionViewModel.get(uid: authViewModel.userSession?.uid)
            }
            
            .navigationBarTitleDisplayMode(.inline)
            .toolbar(content: {
                ToolbarItem(placement: .principal, content: {
                    Text("Occasions")
                })
                                
                ToolbarItem(placement: .navigationBarLeading) {
                    Button(action: {
                        activeSheet = .second
                    }) {
                        Image(systemName: "gearshape")
                    }
                    
                } //: ToolbarItem
            }) //: .toolbar
            
            .sheet(item: $activeSheet) { item in
                switch item {
                case .first:
                    AddOccasionView()
                case .second:
                    SettingsView()
                }
                
            } //: .sheet
            
        } //: NavigationView
    } //: Body
}

// MARK: - Preview

//struct OccasionsView_Previews: PreviewProvider {
//    static var previews: some View {
//        OccasionsView()
//    }
//}
