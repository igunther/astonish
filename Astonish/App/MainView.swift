//
//  MainView.swift
//  Astonish
//
//  Created by Øystein Günther on 17/12/2020.
//

import SwiftUI

struct MainView: View {
    
    // MARK: - Properties
    
    @StateObject var authViewModel = AuthViewModel()
    @State var selectedIndex: Int = 0
    
    // MARK: - Body
    
    var body: some View {
        
        if authViewModel.userSession != nil {
            
            TabView(selection: $selectedIndex) {
                OccasionsView()
                    .tabItem {
                        Image(systemName: "calendar")
                        Text("Occasions")
                    }
                    .tag(0)
                
                GiftsView()
                    .tabItem {
                        Image(systemName: "gift")
                        Text("Gifts")
                    }
                    .tag(1)
                
                ToMeView().environmentObject(authViewModel)
                    .tabItem {
                        Image(systemName: "face.smiling")
                        Text("To me")
                    }
                    .tag(2)
                
                WishListsView()
                    .tabItem {
                        Image(systemName: "heart.text.square")
                        Text("Wishlists")
                    }
                    .tag(3)
                
                PeopleView()
                    .tabItem {
                        Image(systemName: "person.2")
                        Text("People")
                    }
                    .tag(4)
                
            } //: TabView
        } else {
            LoginView()
        } //: if
        
    } //: Body
}

// MARK: - Preview

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}

