//
//  GiftsView.swift
//  Astonish
//
//  Created by Øystein Günther on 10/12/2020.
//

import SwiftUI

struct GiftsView: View {
    
    // MARK: - Properties
    
    @State internal var searchText: String = ""
    @State private var showingAddGiftView: Bool = false
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var giftViewModel = GiftViewModel()
    
    let haptics = UIImpactFeedbackGenerator(style: .medium)
    
    @State private var isGridViewActive: Bool = false
    @State private var gridLayout: [GridItem] = [ GridItem(.flexible()) ]
    @State private var gridColumn: Int = 1
    @State private var toolbarIcon: String = "square.grid.2x2"
    
    // MARK: - Functions
    
    func gridSwitch() {
        if isGridViewActive {
            gridLayout = Array(repeating: .init(.flexible()), count: gridLayout.count % 3 + 1)
            gridColumn = gridLayout.count
        }
        print("Grid Number: \(gridColumn)")
        
        // TOOLBAR IMAGE
        switch gridColumn {
        case 1:
            toolbarIcon = "square.grid.2x2"
        case 2:
            toolbarIcon = "square.grid.3x2"
        case 3:
            toolbarIcon = "rectangle.grid.1x2"
        default:
            toolbarIcon = "square.grid.2x2"
        }
    }
    
    // MARK: - Body
    
    var body: some View {
        NavigationView {
            
            ZStack(alignment: .bottomTrailing) {
                VStack(alignment: .leading) {
                    SearchBarView(text: $searchText)
                        .padding()
                    
                    if !isGridViewActive {
                        
                        List {
                            ForEach(giftViewModel.giftGroups) { giftGroup in
                                
                                let giftsInGroup = giftViewModel.giftsIn(giftGroup.name)
                                
                                if giftsInGroup.count > 0 {
                                    
                                    if let systemImageName = GiftState(rawValue: giftGroup.name)?.systemImage {
                                        
                                        Section(header: HStack { Image(systemName: systemImageName); Text(giftGroup.name) } ) {
                                            
                                            ForEach(giftsInGroup) { gift in
                                                NavigationLink(
                                                    destination: GiftDetailView(gift: gift),
                                                    label: {
                                                        GiftCellWithRecipient(gift: gift)
                                                    })
                                            } //: ForEach
                                        } //: Section
                                    } //: If let
                                } //: If
                            } //: ForEach
                        } //: List
                        //.listStyle(PlainListStyle())
                        .listStyle(InsetGroupedListStyle())
                        
                        /*
                         List(viewModel.gifts) { gift in
                         NavigationLink(destination: GiftDetailView(gift: gift)) {
                         GiftCell(gift: gift)
                         }
                         } //: List
                         .listStyle(PlainListStyle())
                         */
                    } else {
                        ScrollView(.vertical, showsIndicators: false) {
                            LazyVGrid(columns: gridLayout, alignment: .center, spacing: 10) {
                                ForEach(giftViewModel.gifts) { gift in
                                    NavigationLink(destination: GiftDetailView(gift: gift)) {
                                        GiftGridItemView(gift: gift)
                                    } //: Link
                                } //: ForEach
                            } //: Grid
                            .padding(10)
                            .animation(.easeIn)
                        } //: Scroll
                    } //: Condition
                } //: VStack
                
                RoundActionButton(systemImage: "gift") {
                    self.showingAddGiftView.toggle()
                }
                
            } //: ZStack
            
            .sheet(isPresented: $showingAddGiftView) {
                AddGiftView()
            } //: Sheet
            
            .onAppear() {
                self.giftViewModel.get(uid: authViewModel.userSession?.uid)
            }
            
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                
                ToolbarItem(placement: .principal) {
                    Text("Gifts")
                }
                
                ToolbarItem(placement: .navigationBarTrailing) {
                    HStack(spacing: 16) {
                        // List
                        Button(action: {
                            print("List view is activated")
                            isGridViewActive = false
                            haptics.impactOccurred()
                        }) {
                            Image(systemName: "square.fill.text.grid.1x2")
                                .font(.title2)
                                .foregroundColor(isGridViewActive ? .primary : .accentColor)
                        }
                        
                        // Grid
                        Button(action: {
                            print("Grid view is activated")
                            
                            haptics.impactOccurred()
                            gridSwitch()
                            isGridViewActive = true
                        }) {
                            Image(systemName: toolbarIcon)
                                .font(.title2)
                                .foregroundColor(isGridViewActive ? .accentColor : .primary)
                        }
                    } //: HStack
                } //: Buttons
            } //: Toolbar
        } //: NavigationView
    } //: Body
}

// MARK: - Preview

struct GiftsView_Previews: PreviewProvider {
    static var previews: some View {
        GiftsView()
    }
}
