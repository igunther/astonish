//
//  OccasionGifts.swift
//  Astonish
//
//  Created by Øystein Günther on 28/12/2020.
//

import SwiftUI

struct OccasionGifts: View {
    
    // MARK: - Properties
    
    @State internal var occasion: Occasion
    @ObservedObject var giftViewModel = GiftViewModel()
    
    // MARK: - Body
    
    var body: some View {
        
        List(giftViewModel.gifts) { gift in
            
            NavigationLink(
                destination: GiftDetailView(gift: gift),
                label: {
                    GiftCellWithState(gift: gift)
                })
        }
        
        .navigationBarTitle(occasion.name, displayMode: .inline)
        .onAppear() {
            self.giftViewModel.get(occasionId: occasion.id)
        }
    } //: Body
}

// MARK: - Previews

struct OccasionGifts_Previews: PreviewProvider {
    static var previews: some View {
        let occasion = Occasion(dictionary: ["id" : "1", "name": "Some gift"])
        OccasionGifts(occasion: occasion)
    }
}
