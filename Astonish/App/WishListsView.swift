//
//  WishListsView.swift
//  Astonish
//
//  Created by Øystein Günther on 17/12/2020.
//

import SwiftUI

struct WishListsView: View {
    
    // MARK: - Properties
    
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var wishListViewModel = WishListViewModel()
    @State private var showingAddWishView: Bool = false
    @State private var who: String = "People"
    let people = ["People", "You"]
    
    @State var stepperValue: Int = 1
    
    // MARK: - Body
    
    var body: some View {
        NavigationView {
            
            ZStack(alignment: .bottomTrailing) {
                VStack {
                    
                    Picker("People", selection: $who) {
                        ForEach(people, id: \.self) {
                            Text($0)
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                    .padding()
                    
                    //Spacer()
                    
                    List(wishListViewModel.wishLists) { wishList in
                        NavigationLink(destination: WishListDetailView(wishList: wishList)) {
                            WishListCell(wishList: wishList)
                        }
                    }
                    .listStyle(InsetGroupedListStyle())
                    
                    //                Stepper(value: $stepperValue, in: 1...10) {
                    //                    Text("\(stepperValue)")
                    //                }
                    
                } //: VStack
                RoundActionButton(systemImage: "heart.text.square") {
                    self.showingAddWishView.toggle()
                }
            } //: ZStack
            
            .onAppear() {
                self.wishListViewModel.get(uid: authViewModel.userSession?.uid)
            }
            
            .sheet(isPresented: $showingAddWishView) {
                AddWishListView()
            }
                        
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                
                ToolbarItem(placement: .principal) {
                    Text("Wishlists")
                }
            }
            
        } //: NavigationView
    } //: Body
}

// MARK: - Preview

struct WishListsView_Previews: PreviewProvider {
    static var previews: some View {
        WishListsView()
    }
}
