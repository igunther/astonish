//
//  AstonishApp.swift
//  Astonish
//
//  Created by Øystein Günther on 25/11/2020.
//

import SwiftUI
import Firebase
import SwiftyBeaver

let swiftyBeaverLog = SwiftyBeaver.self

@main
struct AstonishApp: App {
    
    init() {
        setupTheme()
        firebaseInit()
        swiftyBeaverInit()
    }
    
    var body: some Scene {
        WindowGroup {
            MainView().environmentObject(AuthViewModel()) // What does this mean?
        }
    }
    
    private func setupTheme() {
        // Init something
    }
    
    private func firebaseInit() {
        FirebaseApp.configure()
    }
    
    private func swiftyBeaverInit() {
        // Add log destinations, at least one is needed!
        let console = ConsoleDestination()  // Log to XCode Console
        let file = FileDestination()        // Log to default swiftybeaver.log file
        
        // Add the destinations to SwiftyBeaver
        swiftyBeaverLog.addDestination(console)
        swiftyBeaverLog.addDestination(file)
  
        var target: String = ""
        var appId: String = ""
        var appSecret: String = ""
        var encryptionKey: String = ""
        
        #if TEST
            target = "TEST"
            appId = "1P9mZe"
            appSecret = "eoOELCvlyXdbfrekfWemswI4ul8ndlen"
            encryptionKey = "0tjNapbhszip0i0ugrqwomapkj9juuVX"
        #else
            target = "PROD"
            appId = "v6gQba"
            appSecret = "ebml3jr6k7wn5ugJ6VeaqJTyNGqoruej"
            encryptionKey = "h2svwl8d5qSh58PN8w9bL5jhdqfhq3il"
        #endif
        
        let swiftyBeaverCloud = SBPlatformDestination(appID: appId, appSecret: appSecret, encryptionKey: encryptionKey)
        
        swiftyBeaverLog.addDestination(swiftyBeaverCloud)
            
        
        swiftyBeaverCloud.analyticsUserName = "Device: \(UIDevice.current.name) (\(target) - \(Bundle.version()))"
        
        //swiftyBeaverCloud.analyticsUserName = "Device: \(UIDevice.current.name), User: \(currentUser.fullName())"
        
        
        
        
//            if let bundleVersion = UIApplication.bundleVersion {
//                swiftyBeaverCloud.analyticsUserName = "Device: \(UIDevice.current.name) (\(target) - \(bundleVersion))"
//            } else {
//                swiftyBeaverCloud.analyticsUserName = "Device: \(UIDevice.current.name) (\(target)"
//            }
//        }
        
    }
}
