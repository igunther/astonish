//
//  ToMeView.swift
//  Astonish
//
//  Created by Øystein Günther on 26/01/2021.
//

import SwiftUI

struct ToMeView: View {
    
    // MARK: - Properties
    
    @State internal var searchText: String = ""
    @State private var showingAddToMeView: Bool = false
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var toMeViewModel = ToMeViewModel()
    
    let haptics = UIImpactFeedbackGenerator(style: .medium)
    
    @State private var isGridViewActive: Bool = false
    @State private var gridLayout: [GridItem] = [ GridItem(.flexible()) ]
    @State private var gridColumn: Int = 1
    @State private var toolbarIcon: String = "square.grid.2x2"
    
    // MARK: - Functions
    
    func gridSwitch() {
        if isGridViewActive {
            gridLayout = Array(repeating: .init(.flexible()), count: gridLayout.count % 3 + 1)
            gridColumn = gridLayout.count
        }
        print("Grid Number: \(gridColumn)")
        
        // TOOLBAR IMAGE
        switch gridColumn {
        case 1:
            toolbarIcon = "square.grid.2x2"
        case 2:
            toolbarIcon = "square.grid.3x2"
        case 3:
            toolbarIcon = "rectangle.grid.1x2"
        default:
            toolbarIcon = "square.grid.2x2"
        }
    }
    
    // MARK: - Body
    
    var body: some View {
        NavigationView {
            
            ZStack(alignment: .bottomTrailing) {
                VStack(alignment: .leading) {
                    SearchBarView(text: $searchText)
                        .padding()
                    
                    if !isGridViewActive {
                        
                        List {
                            ForEach(toMeViewModel.occasions) { occasionGroup in
                                
                                let toMeInOccasions = toMeViewModel.toMeIn(occasionGroup.name)
                                
                                Section(header: Text(occasionGroup.name) ) {
                                    ForEach(toMeInOccasions) { toMe in
                                        NavigationLink (
                                            destination: ToMeDetailView(toMe: toMe),
                                            label: {
                                                ToMeCellWithSender(toMe: toMe)
                                        })
                                    } //: ForEach
                                } //: Section
                            } //: ForEach
                        } //: List
                        //.listStyle(PlainListStyle())
                        .listStyle(InsetGroupedListStyle())
                    } else {
                        ScrollView(.vertical, showsIndicators: false) {
                            LazyVGrid(columns: gridLayout, alignment: .center, spacing: 10) {
                                ForEach(toMeViewModel.giftsToMe) { toMe in
                                    NavigationLink(destination: ToMeDetailView(toMe: toMe)) {
                                        ToMeGridItemView(toMe: toMe)
                                    } //: Link
                                } //: ForEach
                            } //: Grid
                            .padding(10)
                            .animation(.easeIn)
                        } //: Scroll
                    } //: Condition
                } //: VStack
                
                RoundActionButton(systemImage: "face.smiling") {
                    self.showingAddToMeView.toggle()
                }
                
            } //: ZStack
            
            .sheet(isPresented: $showingAddToMeView) {
                AddToMeView()
            } //: Sheet
            
            .onAppear() {
                self.toMeViewModel.get(uid: authViewModel.userSession?.uid)
            }
            
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                
                ToolbarItem(placement: .principal) {
                    Text("To me")
                }
                
                ToolbarItem(placement: .navigationBarTrailing) {
                    HStack(spacing: 16) {
                        // List
                        Button(action: {
                            print("List view is activated")
                            isGridViewActive = false
                            haptics.impactOccurred()
                        }) {
                            Image(systemName: "square.fill.text.grid.1x2")
                                .font(.title2)
                                .foregroundColor(isGridViewActive ? .primary : .accentColor)
                        }
                        
                        // Grid
                        Button(action: {
                            print("Grid view is activated")
                            
                            haptics.impactOccurred()
                            gridSwitch()
                            isGridViewActive = true
                        }) {
                            Image(systemName: toolbarIcon)
                                .font(.title2)
                                .foregroundColor(isGridViewActive ? .accentColor : .primary)
                        }
                    } //: HStack
                } //: Buttons
            } //: Toolbar
        } //: NavigationView
    } //: Body
}

struct ToMeView_Previews: PreviewProvider {
    static var previews: some View {
        ToMeView()
            .previewDevice("iPhone 11 Pro")
    }
}
