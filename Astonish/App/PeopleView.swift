//
//  PeopleView.swift
//  Astonish
//
//  Created by Øystein Günther on 17/12/2020.
//

import SwiftUI

struct PeopleView: View {
    
    // MARK: - Properties
    
    @State private var showingAddPeopleSheet = false
    @State private var showingAddPersonView: Bool = false
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var peopleViewModel = PeopleViewModel()
    
    // MARK: - Body
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .bottomTrailing) {
                VStack(alignment: .leading) {
                    List {
                        if peopleViewModel.personGroups.count == 0 {
                            
                            ForEach(peopleViewModel.people) { person in
                                NavigationLink(
                                    destination: PersonDetailView(person: person),
                                    label: {
                                        PersonCell(person: person)
                                    })
                            }
                            
                        } else {
                            
                            // Display all persons in their respective groups.
                            ForEach(peopleViewModel.personGroups) { group in
                                
                                Section(header: Text(group.name)) {
                                    
                                    let peopleInGroup = peopleViewModel.peopleIn(group.name)
                                    
                                    ForEach(peopleInGroup) { person in
                                        NavigationLink(
                                            destination: PersonDetailView(person: person),
                                            label: {
                                                PersonCell(person: person)
                                            })
                                    }
                                }
                            }
                        }
                    } //: List
                    .listStyle(InsetGroupedListStyle())
                    
                } //: VStack
                
                RoundActionButton(systemImage: "person.badge.plus") {
                    self.showingAddPeopleSheet = true
                }
                
            } //: ZStack
            
            .actionSheet(isPresented: $showingAddPeopleSheet, content: {
                ActionSheet(title: Text("Add People"), buttons: [.default(Text("New person")) { self.showingAddPersonView.toggle() },
                                                                 .default(Text("Import from Contacts")) { swiftyBeaverLog.debug("Import from Contacts") },
                                                                 .cancel()])
            })
            .sheet(isPresented: $showingAddPersonView, content: {
                AddPersonView()
            })
            
            .onAppear() {
                self.peopleViewModel.fetchPeople(uid: authViewModel.userSession?.uid)
            }
            
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                
                ToolbarItem(placement: .principal) {
                    Text("People")
                }
            }
            
        } //: NavigationView
    } //: Body
}

// MARK: - Preview

struct PeopleView_Previews: PreviewProvider {
    static var previews: some View {
        PeopleView()
    }
}
