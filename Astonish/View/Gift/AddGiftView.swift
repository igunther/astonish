//
//  AddGiftView.swift
//  Astonish
//
//  Created by Øystein Günther on 29/12/2020.
//

import SwiftUI

struct AddGiftView: View {
    
    // MARK: - Properties
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var giftViewModel = GiftViewModel()
    
    @State private var name: String = ""
    @State private var selectedOccasionName: String = "Optional"
    @State private var selectedOccasionId: String = ""
    @State private var image: Image?
    @State private var selectedUIImage: UIImage?
    @State private var showActionSheet = false
    @State private var showImagePicker = false
    @State private var sourceType: UIImagePickerController.SourceType = .camera
    @State private var profileImageUrl: String?
    @State var selectedRecipients = Set<Recipient>()
    @State private var selectedReaction = REACTION_THUMBS_UP
    @State private var selectedGiftState = GiftState.idea.rawValue
    
    private var reactions = [REACTION_ASTONISHED, REACTION_THUMBS_UP, REACTION_THUMBS_DOWN]
            
    func loadImage() {
        guard let selectedImage = selectedUIImage else {  return }
        image = Image(uiImage: selectedImage)
    }
    
    // MARK: - Body
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                
                VStack(spacing: 20) {
                    
                    // MARK: - Section 1
                    
                    GroupBox(
                        label:
                            Picker(GiftState.idea.rawValue, selection: $selectedGiftState) {
                                Image(systemName: GiftState.idea.systemImage).tag(GiftState.idea.rawValue)
                                Image(systemName: GiftState.toBuy.systemImage).tag(GiftState.toBuy.rawValue)
                                Image(systemName: GiftState.ordered.systemImage).tag(GiftState.ordered.rawValue)
                                Image(systemName: GiftState.toWrap.systemImage).tag(GiftState.toWrap.rawValue)
                                Image(systemName: GiftState.wrapped.systemImage).tag(GiftState.wrapped.rawValue)
                                Image(systemName: GiftState.gifted.systemImage).tag(GiftState.gifted.rawValue)
                            } //: Picker
                            .pickerStyle(SegmentedPickerStyle())
                            .onChange(of: selectedGiftState, perform: { _ in
                              print("change state: \(selectedGiftState)")
                            })
                    ) {
                        Divider().padding(.vertical, 4)
                        
                        HStack(alignment: .center, spacing: 10) {
                            
                            Button(action: { showActionSheet.toggle() }, label: {
                                
                                ZStack {
                                    if let image = image {
                                        image
                                            .resizable()
                                            .scaledToFill()
                                            .clipped()
                                            .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X)
                                            .clipShape(Circle())
                                            .overlay(Circle().stroke(Color.white, lineWidth: 1))
                                    }
                                    else {
                                        Image(systemName: "gift")
                                            .resizable()
                                            .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X, alignment: .center)
                                            .font(Font.title.weight(.ultraLight))
                                            .scaleEffect(0.7)
                                            .clipShape(Circle())
                                            .overlay(Circle().stroke(Color.white, lineWidth: 1))
                                    }
                                } //: ZStack
                            }) //: Button
                            
                            VStack {
                                TextField("Name", text: $name)
                                Divider().padding(.vertical, 4)
                                
                                HStack {
                                    NavigationLink(
                                        destination: OccasionSelectView(selectedOccasionId: self.$selectedOccasionId, selectedOccasionName: self.$selectedOccasionName),
                                        label: {
                                            Text("Occasion")
                                            Spacer()
                                            
                                            Text(selectedOccasionName)
                                                .foregroundColor(Color(.systemGray2))
                                            Image(systemName: "chevron.right")
                                                .foregroundColor(Color(.systemGray2))
                                        })
                                } //: HStack
                                
                                Divider().padding(.vertical, 4)
                                
                                // Picker("People", selection: $who)
                                Picker("hand.thumbsup", selection: $selectedReaction) {
                                    ForEach(0 ..< reactions.count) {
                                        Image(systemName: self.reactions[$0])
                                            .tag(self.reactions[$0])
                                    }
                                }
                                .pickerStyle(SegmentedPickerStyle())
                            } //: VStack
                        } //: HStack
                    } //: GroupBox
                    
                    // MARK: - Section 2
                    
                    Group {
                        
                        HStack {
                            
                            NavigationLink(
                                destination: RecipientSelectView(selectedRecipients: $selectedRecipients),
                                label: {
                                    Text("Recipient")
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .background(Color(.systemBackground))
                                    
                                    Spacer()
                                    
                                    if selectedRecipients.count == 0 {
                                        Text("Optional")
                                            .foregroundColor(Color(.systemGray2))
                                    } else if selectedRecipients.count == 1 {
                                        if let recipient = selectedRecipients.first {
                                            Text(recipient.fullName)
                                        }
                                    } else {
                                        Text("Multiple...")
                                    }
                                    
                                    Image(systemName: "chevron.right")
                                        .foregroundColor(Color(.systemGray2))
                                    
                                }) //: NavigationLink
                        } //: HStack
                        
                        Divider().padding(.vertical, 4)
                        
                    }
                    
                    .sheet(isPresented: $showImagePicker, onDismiss: loadImage, content: {
                        ImagePicker(image: $selectedUIImage, sourceType: self.sourceType)
                    })
                    
                    .actionSheet(isPresented: $showActionSheet, content: {
                        ActionSheet(title: Text("Photo"), buttons: [
                            .default(Text("Photo Library")) {
                                self.showImagePicker = true
                                self.sourceType = .photoLibrary
                            },
                            .default(Text("Camera")) {
                                #if targetEnvironment(simulator)
                                #else
                                self.showImagePicker = true
                                self.sourceType = .camera
                                #endif
                            },
                            .cancel()
                        ])
                    })
                } //: VStack
                
            } //: ScrollView
            .padding()
            
            .navigationBarTitle("New Gift", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        Text("Cancel")
                                    }, trailing:
                                        Button(action: {
                                            addGift()
                                            self.presentationMode.wrappedValue.dismiss()
                                        }) {
                                            Text("Save")
                                        }
            ) //: NavigationBarItems
        } //: NavigationView
    } //: Body
    
    // MARK: - Functions
    
    private func addGift() {
        guard let uid = authViewModel.userSession?.uid else { return }
           
        let recipientsIds = selectedRecipients.map { $0.id }
        
        var recipient: Recipient?
        if selectedRecipients.count == 1 {
            if let firstRecipient = selectedRecipients.first {
                recipient = Recipient(id: firstRecipient.id, firstName: firstRecipient.firstName, lastName: firstRecipient.lastName)
            }
        }
        
        let gift = Gift(uid: uid, occasionId: selectedOccasionId, occasionName: selectedOccasionName, giftState: selectedGiftState, name: name, recipient: recipient, recipients: recipientsIds, reaction: selectedReaction)
        
        giftViewModel.add(gift: gift, giftImage: selectedUIImage)
    }
}

// MARK: - Preview

struct AddGiftView_Previews: PreviewProvider {
    static var previews: some View {
        AddGiftView()
    }
}
