//
//  GiftDetailView.swift
//  Astonish
//
//  Created by Øystein Günther on 09/01/2021.
//

import SwiftUI
import struct Kingfisher.KFImage

struct GiftDetailView: View {
    
    // MARK: - Properties
    
    var gift: Gift {
        didSet {
            // selectedReceipients
            
            //            guard let recipeints = gift.
            //            @StateObject var peopleViewModel = PeopleViewModel()
            //            peopleViewModel.getPeople(idArray: <#T##[String]#>)
            
        }
    }
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var authViewModel: AuthViewModel 
    @StateObject var giftViewModel = GiftViewModel()
    @StateObject var peopleViewModel = PeopleViewModel()
    
    @State private var name: String = ""
    @State private var selectedOccasionName: String = ""
    @State private var selectedOccasionId: String = ""
    @State private var image: Image?
    @State private var selectedUIImage: UIImage?
    @State private var showActionSheet = false
    @State private var showImagePicker = false
    @State private var sourceType: UIImagePickerController.SourceType = .camera
    @State private var giftImageUrl: String = ""
    @State var selectedRecipients = Set<Recipient>()
    @State private var selectedReaction = ""
    @State private var selectedGiftState = GiftState.idea.rawValue
    @State private var giftStateChanged: Bool = false
    @State private var isAnimating: Bool = false
    
    var reactions = [REACTION_ASTONISHED, REACTION_THUMBS_UP, REACTION_THUMBS_DOWN]
        
    func loadImage() {
        guard let selectedImage = selectedUIImage else {  return }
        image = Image(uiImage: selectedImage)
    }
    
    // MARK: - Functions
    
    // MARK: - Body
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 20) {
                
                // MARK: - Section 1
                
                GroupBox (
                    label:
                        Picker(GiftState.idea.rawValue, selection: $selectedGiftState) {
                            Image(systemName: GiftState.idea.systemImage).tag(GiftState.idea.rawValue)
                            Image(systemName: GiftState.toBuy.systemImage).tag(GiftState.toBuy.rawValue)
                            Image(systemName: GiftState.ordered.systemImage).tag(GiftState.ordered.rawValue)
                            Image(systemName: GiftState.toWrap.systemImage).tag(GiftState.toWrap.rawValue)
                            Image(systemName: GiftState.wrapped.systemImage).tag(GiftState.wrapped.rawValue)
                            Image(systemName: GiftState.gifted.systemImage).tag(GiftState.gifted.rawValue)
                        } //: Picker
                        .pickerStyle(SegmentedPickerStyle())
                        .onChange(of: selectedGiftState, perform: { _ in
                            giftStateChanged = true
                        })
                ) //: GroupBox
                {
                    Divider().padding(.vertical, 4)
                    
                    HStack(alignment: .center, spacing: 10) {
                        
                        Button(action: { showActionSheet.toggle() }, label: {
                            
                            ZStack {
                                if let image = image {
                                    image
                                        .resizable()
                                        .scaledToFill()
                                        .clipped()
                                        .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X)
                                        .clipShape(Circle())
                                        .overlay(Circle().stroke(Color.white, lineWidth: 1))
                                }
                                else if let giftImageUrl = giftImageUrl, let url = URL(string: giftImageUrl) {
                                    KFImage(url)
                                        .resizable()
                                        .scaledToFill()
                                        .clipped()
                                        .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X)
                                        .clipShape(Circle())
                                        .overlay(Circle().stroke(Color.white, lineWidth: 1))
                                        //.padding()
                                        .scaleEffect(isAnimating ? 1.0 : 0.6)
                                }
                                else {
                                    Image(systemName: "gift")
                                        .resizable()
                                        .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X, alignment: .center)
                                        .font(Font.title.weight(.ultraLight))
                                        .scaleEffect(0.7)
                                        .clipShape(Circle())
                                        .overlay(Circle().stroke(Color.white, lineWidth: 1))
                                }
                            } //: ZStack
                        }) //: Button
                        .onAppear {
                            withAnimation(.easeOut(duration: 1.0)) {
                                isAnimating = true
                            }
                        }
                        
                        VStack {
                            TextField(gift.name, text: $name)
                            Divider().padding(.vertical, 4)
                            
                            HStack {
                                NavigationLink(
                                    destination: OccasionSelectView(selectedOccasionId: self.$selectedOccasionId, selectedOccasionName: self.$selectedOccasionName),
                                    label: {
                                        Text("Occasion")
                                        Spacer()
                                        
                                        Text(selectedOccasionName.isEmpty ? "Optional" : selectedOccasionName)
                                            .foregroundColor(Color(.systemGray2))
                                        Image(systemName: "chevron.right")
                                            .foregroundColor(Color(.systemGray2))
                                    })
                            } //: HStack
                            
                            Divider().padding(.vertical, 4)
                            
                            Picker(REACTION_THUMBS_UP, selection: $selectedReaction) {
                                ForEach(0 ..< reactions.count) {
                                    Image(systemName: self.reactions[$0])
                                        .tag(self.reactions[$0])
                                }
                            } //: Picker
                            .pickerStyle(SegmentedPickerStyle())
                        } //: VStack
                    } //: HStack
                } //: GroupBox
                
                // MARK: - Section 2
                
                Group {
                    
                    HStack {
                        
                        NavigationLink(
                            destination: RecipientSelectView(selectedRecipients: $selectedRecipients),
                            label: {
                                Text("Recipient")
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .background(Color(.systemBackground))
                                
                                Spacer()
                                
                                if selectedRecipients.count == 0 {
                                    Text("Optional")
                                        .foregroundColor(Color(.systemGray2))
                                } else if selectedRecipients.count == 1 {
                                    if let recipient = selectedRecipients.first {
                                        Text(recipient.fullName)
                                    }
                                } else {
                                    Text("Multiple...")
                                }
                                
                                Image(systemName: "chevron.right")
                                    .foregroundColor(Color(.systemGray2))
                                
                            }) //: NavigationLink
                    } //: HStack
                    
                    Divider().padding(.vertical, 4)
                    
                }
                
                .sheet(isPresented: $showImagePicker, onDismiss: loadImage, content: {
                    ImagePicker(image: $selectedUIImage, sourceType: self.sourceType)
                })
                
                .actionSheet(isPresented: $showActionSheet, content: {
                    ActionSheet(title: Text("Photo"), buttons: [
                        .default(Text("Photo Library")) {
                            self.showImagePicker = true
                            self.sourceType = .photoLibrary
                        },
                        .default(Text("Camera")) {
                            #if targetEnvironment(simulator)
                            #else
                            self.showImagePicker = true
                            self.sourceType = .camera
                            #endif
                        },
                        .cancel()
                    ])
                })
            } //: VStack
            
        } //: ScrollView
        .padding()
        
        .navigationBarTitle("", displayMode: .inline)
        .navigationBarItems(trailing:
                                Button(action: {
                                    updateGift()
                                    self.presentationMode.wrappedValue.dismiss()
                                }) {
                                    Text("Save")
                                }
        ) //: NavigationBarItem
        
        .onAppear() {
            
            initStateVariables()
            
            //getRecipients()
        }
    } //: Body
    
    // MARK: - Functions
    
    private func initStateVariables() {
        
        if giftImageUrl.isEmpty {
            if let storedGiftImageUrl = gift.giftImageUrl {
                giftImageUrl = storedGiftImageUrl
            }
        }
        
        if name.isEmpty {
            name = gift.name
        }
        
        if selectedOccasionId.isEmpty {
            if let occasionId = gift.occasionId {
                selectedOccasionId = occasionId
            }
        }
        
        if selectedOccasionName.isEmpty {
            if let occasionName = gift.occasionName {
                selectedOccasionName = occasionName
            }
        }
        
        if !giftStateChanged {
            selectedGiftState = gift.giftState
        }
        
        if selectedReaction.isEmpty {
            selectedReaction = gift.reaction
        }
        
        if selectedRecipients.count == 0 {
            if let recipients = gift.recipients {
                if recipients.count == 1 {
                    // No need to fetch recipients when there is only one.
                    if let recipient = gift.recipient {
                        let recipient = Recipient(id: recipient.id, firstName: recipient.firstName, lastName: recipient.lastName)
                        selectedRecipients.insert(recipient)
                    }
                } else if recipients.count > 1 {
                    swiftyBeaverLog.debug("recipients.count: \(recipients.count)")
                    
                    peopleViewModel.getRecipients(recipientIds: recipients) { giftRecipients in
                        if let giftRecipients = giftRecipients {
                            swiftyBeaverLog.debug("giftRecipients.count: \(giftRecipients.count)")
                            
                            for recipient in giftRecipients {
                                swiftyBeaverLog.verbose("recipient: \(recipient.fullName)")
                                selectedRecipients.insert(recipient)
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    private func updateGift() {
        guard let uid = authViewModel.userSession?.uid else { return }
        
        giftViewModel.update(
            uid: uid,
            id: gift.id,
            name: name,
            occasionId: selectedOccasionId,
            occasionName: selectedOccasionName,
            giftState: selectedGiftState,
            reaction: selectedReaction,
            recipients: selectedRecipients,
            giftImage: selectedUIImage,
            giftImageId: gift.giftImageId)
    }
}

//struct GiftDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        GiftDetailView()
//    }
//}
