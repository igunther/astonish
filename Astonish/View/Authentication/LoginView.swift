//
//  LoginView.swift
//  Astonish
//
//  Created by Øystein Günther on 20/12/2020.
//

import SwiftUI

struct LoginView: View {
    
    // MARK: - Properties
    
    @State var email = ""
    @State var password = ""
    @EnvironmentObject var viewModel: AuthViewModel
    
    // MARK: - Body
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    Image("launch-screen-image")
                        .resizable()
                        .scaledToFill()
                        .frame(width: 100, height: 100)
                        .padding(.top, 60)
                        .padding(.bottom, 40)
                    
                    VStack(spacing: 20) {
                        CustomTextField(text: $email, placeholder: Text("Email"), imageName: "envelope")
                            .padding()
                            .background(Color(.init(white: 1, alpha: 0.15)))
                            .cornerRadius(10)
                        
                        CustomSecureTextField(text: $password, placeholder: Text("Password"))
                            .padding()
                            .background(Color(.init(white: 1, alpha: 0.15)))
                            .cornerRadius(10)
                        
                    } //: VStack
                    .padding(.horizontal, 32)
                    
                    HStack {
                        Spacer()
                        Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                            Text("Forgot Password?")
                                .font(.footnote)
                                .bold()
                                .padding(.top, 16)
                                .padding(.trailing, 32)
                        })
                        
                    } //: HStack
                    
                    Button(action: {
                        viewModel.login(withEmail: email, password: password)
                    }, label: {
                        Text("Sign In")
                            .padding()
                            .font(.headline)
                            .frame(width: 320, height: 50)
                            .background(Color(UIColor.systemBlue))
                            .foregroundColor(Color(UIColor.label))
                            .clipShape(Capsule())
                    })
                    
                    Spacer()
                    
                    NavigationLink(
                        destination: RegistrationView().navigationBarHidden(true),
                        label: {
                            HStack {
                                Text("Don´t have an account?")
                                    .font(.system(size: 14))
                                
                                Text("Sign Up")
                                    .font(.system(size: 14, weight: .semibold))
                            } //: HStack
                            .padding(.bottom, 40)
                        })
                    
                } //: VStack
            }
        } //: ZStack
        
        //.ignoresSafeArea()
    } //: NavigationView
}

// MARK: - Preview

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

