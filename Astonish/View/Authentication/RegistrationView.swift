//
//  RegistrationView.swift
//  Astonish
//
//  Created by Øystein Günther on 20/12/2020.
//

import SwiftUI

struct RegistrationView: View {
    
    // MARK: - Properties
    @State var email = ""
    @State var password = ""
    @State var firstName = ""
    @State var lastName = ""
    @State var showImagePicker = false
    @State var selectedUIImage: UIImage?
    @State var image: Image?
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: AuthViewModel
    
    func loadImage() {
        guard let selectedImage = selectedUIImage else {  return }
        image = Image(uiImage: selectedImage)
    }
    
    // MARK: - Body
    
    var body: some View {
        ZStack {
            VStack {
                Button(action: { showImagePicker.toggle() }, label: {
                    ZStack {
                        if let image = image {
                            image
                                .resizable()
                                .scaledToFill()
                                .frame(width: 100, height: 100)
                                .clipped()
                                .cornerRadius(50)
                                .padding(.top, 60)
                                .padding(.bottom, 40)
                        } else {
                            Image(systemName: "photo.on.rectangle.angled")
                                .resizable()
                                .scaledToFill()
                                .frame(width: 100, height: 100)
                                .padding(.top, 60)
                                .padding(.bottom, 40)
                        }
                        
                    }
                }).sheet(isPresented: $showImagePicker, onDismiss: loadImage, content: {
                    ImagePicker(image: $selectedUIImage)
                })
                
                VStack(spacing: 20) {
                    CustomTextField(text: $email, placeholder: Text("Email"), imageName: "envelope")
                        .autocapitalization(.none)
                        .padding()
                        .background(Color(.init(white: 1, alpha: 0.15)))
                        .cornerRadius(10)
                    
                    CustomTextField(text: $firstName, placeholder: Text("First name"), imageName: "person")
                        .padding()
                        .background(Color(.init(white: 1, alpha: 0.15)))
                        .cornerRadius(10)
                    
                    CustomTextField(text: $lastName, placeholder: Text("Last name"), imageName: "person")
                        .padding()
                        .background(Color(.init(white: 1, alpha: 0.15)))
                        .cornerRadius(10)
                    
                    CustomSecureTextField(text: $password, placeholder: Text("Password"))
                        .padding()
                        .background(Color(.init(white: 1, alpha: 0.15)))
                        .cornerRadius(10)
                    
                } //: VStack
                .padding(.horizontal, 32)
                
                Button(action: {
                    guard let image = selectedUIImage else { return }
                    viewModel.registerUser(email: email, password: password, firstName: firstName, lastName: lastName, profileImage: image)
                }, label: {
                    Text("Sign Up")
                        .padding()
                        .font(.headline)
                        .frame(width: 320, height: 50)
                        .background(Color(UIColor.systemBlue))
                        .foregroundColor(Color(UIColor.label))
                        .clipShape(Capsule())
                        .padding()
                })
                
                Spacer()
                
                Button(action: { mode.wrappedValue.dismiss() }, label: {
                    HStack {
                        Text("Already have an account?")
                            .font(.system(size: 14))
                        
                        Text("Sign In")
                            .font(.system(size: 14, weight: .semibold))
                    } //: HStack
                    .padding(.bottom, 40)
                })
                
            } //: VStack
        } //: ZStack
        
        //.ignoresSafeArea()
    }
}

// MARK: - Preview

struct RegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView()
    }
}
