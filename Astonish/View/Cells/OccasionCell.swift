//
//  OccasionCell.swift
//  Astonish
//
//  Created by Øystein Günther on 26/12/2020.
//

import SwiftUI

struct OccasionCell: View {
    
    // MARK: - Properties
    
    let occasion: Occasion
    
    // MARK: - Body
    
    var body: some View {
        HStack {
            Image("santa-claus-2")
                .renderingMode(.template)
                .resizable()
                .scaledToFill()
                .clipped()
                .foregroundColor(.white)
                .frame(width: 50, height: 50)
                .cornerRadius(25)
                
            VStack(alignment: .leading, spacing: 4) {
                Text(occasion.name)
                    //.foregroundColor(.white)
                
            } //: VStack
        
            Spacer()
            
        } //: HStack
        .background(Color(.gray))
        
    } //: Body
}

// MARK: - Preview

struct OccasionCell_Previews: PreviewProvider {
    static var previews: some View {
        let occasion = Occasion(dictionary: ["Some occasion": "Anything"])
        OccasionCell(occasion: occasion)
    }
}
