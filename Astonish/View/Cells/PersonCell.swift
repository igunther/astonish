//
//  PersonCell.swift
//  Astonish
//
//  Created by Øystein Günther on 01/01/2021.
//

import SwiftUI
import struct Kingfisher.KFImage

struct PersonCell: View {
    
    // MARK: - Properties
    
    let person: Person
    
    
    // MARK: - Body
    
    var body: some View {
        HStack {
            if let profileImageUrl = person.profileImageUrl, let url = URL(string: profileImageUrl) {
                KFImage(url)
                    .resizable()
                    .scaledToFill()
                    .clipped()
                    .frame(width: PROFILE_IMAGE_WIDTH, height: PROFILE_IMAGE_HEIGHT)
                    //.cornerRadius(PROFILE_IMAGE_CORNER_RADIUS)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.white, lineWidth: 1))
            } else {
            Image(systemName: "person.crop.circle")
                .resizable()
                .scaledToFill()
                .clipped()
                .foregroundColor(.white)
                .frame(width: PROFILE_IMAGE_WIDTH, height: PROFILE_IMAGE_HEIGHT)
                .cornerRadius(PROFILE_IMAGE_CORNER_RADIUS)
                .font(Font.title.weight(.ultraLight))
            }
            VStack(alignment: .leading, spacing: 4) {
                Text(person.fullName)
                    //.foregroundColor(.white)
                
            } //: VStack
        
            Spacer()
            
        } //: HStack
        //.background(Color(.gray))
        
    } //: Body
}

struct PersonCell_Previews: PreviewProvider {
    static var previews: some View {
        let personData = ["id": "1", "firstName": "Erlich", "lastName": "Backman"]
        let person = Person(dictionary: personData)
        PersonCell(person: person)
    }
}
