//
//  SenderCell.swift
//  Astonish
//
//  Created by Øystein Günther on 29/01/2021.
//

import SwiftUI

struct SenderCell: View {
    
    // MARK: - Properties
    
    var sender: Sender
    @Binding var selectedItems: Set<Sender>
        
    var isSelected: Bool {
        selectedItems.contains(sender)
    }
    
    // MARK: - Body
    
    var body: some View {
        HStack {
            Text(self.sender.fullName)
                .font(.body)
                // This line is Gold!
                // Expands the text and aligns it to the left, in order to accomplish the side effect of
                // making the whole line "tappable". Setting the background color is necessary!
                .frame(maxWidth: .infinity, alignment: .leading)
                .background(Color(.systemBackground))
            Spacer()
            if self.isSelected {
                Image(systemName: "checkmark")
                    .foregroundColor(Color.blue)
            }
        } //: HStack
        .onTapGesture {
            if self.isSelected {
                self.selectedItems.remove(self.sender)
            } else {
                self.selectedItems.insert(self.sender)
            }
        }
    }
}

/*
struct SenderCell_Previews: PreviewProvider {
    static var previews: some View {
        SenderCell()
    }
}
*/
