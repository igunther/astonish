//
//  RecipientCell.swift
//  Astonish
//
//  Created by Øystein Günther on 08/01/2021.
//

import SwiftUI

struct RecipientCell: View {
    
    // MARK: - Properties
    
    var recipient: Recipient
    @Binding var selectedItems: Set<Recipient>
        
    var isSelected: Bool {
        selectedItems.contains(recipient)
    }
    
    // MARK: - Body
    
    var body: some View {
        HStack {
            Text(self.recipient.fullName)
                .font(.body)
                // This line is Gold!
                // Expands the text and aligns it to the left, in order to accomplish the side effect of
                // making the whole line "tappable". Setting the background color is necessary!
                .frame(maxWidth: .infinity, alignment: .leading)
                .background(Color(.systemBackground))
            Spacer()
            if self.isSelected {
                Image(systemName: "checkmark")
                    .foregroundColor(Color.blue)
            }
        } //: HStack
        .onTapGesture {
            if self.isSelected {
                self.selectedItems.remove(self.recipient)
            } else {
                self.selectedItems.insert(self.recipient)
            }
        }
    }
}

// MARK: - Preview

//struct RecipientCell_Previews: PreviewProvider {
//    static var previews: some View {
//        RecipientCell()
//    }
//}
