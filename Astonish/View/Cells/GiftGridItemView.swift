//
//  GiftGridItemView.swift
//  Astonish
//
//  Created by Øystein Günther on 17/01/2021.
//

import SwiftUI
import struct Kingfisher.KFImage

struct GiftGridItemView: View {
    
    // MARK: - Properties
    
    let gift: Gift
    
    // MARK: - Body
    
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
        if let giftImageUrl = gift.giftImageUrl, let url = URL(string: giftImageUrl) {
            KFImage(url)
                .resizable()
                .scaledToFit()
                .cornerRadius(12)
        }
        else {
            Image(systemName: "gift")
                .resizable()
                .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X, alignment: .center)
                .font(Font.title.weight(.ultraLight))
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.white, lineWidth: 1))
        }
            Image(systemName: GiftState(rawValue: gift.giftState)?.systemImage ?? GiftState.idea.systemImage)
                .padding(.bottom, 5)
                .padding(.trailing, 5)
            } //: ZStack
    }
}

// MARK: - Preview

//struct GiftGridItemView_Previews: PreviewProvider {
//    static var previews: some View {
//        GiftGridItemView()
//            .previewLayout(.sizeThatFits)
//            .padding()
//    }
//}
