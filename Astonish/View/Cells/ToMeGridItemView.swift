//
//  ToMeGridItemView.swift
//  Astonish
//
//  Created by Øystein Günther on 28/01/2021.
//

import SwiftUI
import struct Kingfisher.KFImage

struct ToMeGridItemView: View {
    // MARK: - Properties
    
    let toMe: ToMe
    
    // MARK: - Body
    
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            if let giftImageUrl = toMe.image?.url, let url = URL(string: giftImageUrl) {
            KFImage(url)
                .resizable()
                .scaledToFit()
                .cornerRadius(12)
        }
        else {
            Image(systemName: "gift")
                .resizable()
                .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X, alignment: .center)
                .font(Font.title.weight(.ultraLight))
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.white, lineWidth: 1))
        }
//            Image(systemName: GiftState(rawValue: gift.giftState)?.systemImage ?? GiftState.idea.systemImage)
//                .padding(.bottom, 5)
//                .padding(.trailing, 5)
            } //: ZStack
    }
}

/*
struct ToMeGridItemView_Previews: PreviewProvider {
    static var previews: some View {
        ToMeGridItemView()
    }
}
*/
