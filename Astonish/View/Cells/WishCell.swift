//
//  WishCell.swift
//  Astonish
//
//  Created by Øystein Günther on 11/02/2021.
//

import SwiftUI

struct WishCell: View {
    
    let wish: Wish
        
    var body: some View {
        
        // The Spacer and contentShape makes the whole row tappable.
        
        HStack {
            content
            Spacer()
        }
        .contentShape(Rectangle())
    } //: Body
    
    
    private var content: some View {
        VStack (alignment: .leading) {
            Text(wish.name)
                .font(.headline)
            
            
            VStack(alignment: .leading) {
                Text(wish.url ?? "")
            }
            
            
        }
    }
    
} //: Struct

struct WishCell_Previews: PreviewProvider {
    static var previews: some View {
        
        let wish = Wish(id: "!", wishListId: "!", name: "A wish", note: "Some note", url: "https://www.somestore.com")
        
        WishCell(wish: wish)
            .previewLayout(.sizeThatFits)
    }
}
