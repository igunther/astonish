//
//  GiftCellWithRecipient.swift
//  Astonish
//
//  Created by Øystein Günther on 20/01/2021.
//

import SwiftUI
import struct Kingfisher.KFImage

struct GiftCellWithRecipient: View {
    
    // MARK: - Properties
    
    let gift: Gift
    
    // MARK: - Body
    
    var body: some View {
        HStack {
            
            if let giftImageUrl = gift.giftImageUrl, let url = URL(string: giftImageUrl) {
                KFImage(url)
                    .resizable()
                    .scaledToFill()
                    .clipped()
                    .frame(width: PROFILE_IMAGE_WIDTH, height: PROFILE_IMAGE_HEIGHT)
                    //.cornerRadius(PROFILE_IMAGE_CORNER_RADIUS)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.white, lineWidth: 1))
            } else {
                Image(systemName: "person.crop.circle")
                    .resizable()
                    .scaledToFill()
                    .clipped()
                    .foregroundColor(.white)
                    .frame(width: PROFILE_IMAGE_WIDTH, height: PROFILE_IMAGE_HEIGHT)
                    .cornerRadius(PROFILE_IMAGE_CORNER_RADIUS)
                    .font(Font.title.weight(.ultraLight))
            }
            
            VStack(alignment: .leading, spacing: 4) {
                Text(gift.name)
                
                if gift.recipients?.count == 1 {
                    Text(gift.recipient?.fullName ?? "")
                        .font(.footnote)
                } else {
                    Text("Multiple...")
                        .font(.footnote)
                }
                
            } //: VStack
            
            Spacer()
            
        } //: HStack
    } //: Body
}

// MARK: - Preview

/*
 struct GiftCell_Previews: PreviewProvider {
 static var previews: some View {
 //let occasion = Occasion(dictionary: ["id": "1",
 //                               "name": "Some occasion"])
 let gift = Gift(uid: "1", occasionId: nil, name: "Some Gift", recipient: nil, recipients: nil, reaction: REACTION_THUMBS_UP)
 GiftCellWithRecipient(/*occasion: occasion,*/ gift: gift)
 
 .previewLayout(.sizeThatFits)
 .padding()
 }
 }
 */
