//
//  OccasionListCell.swift
//  Astonish
//
//  Created by Øystein Günther on 29/12/2020.
//

import SwiftUI

struct OccasionListCell: View {
    
    // MARK: - Properties
    
    //@Binding var selectedOccasionName: String?
    //@Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let occasion: Occasion
    
    // MARK: - Body
    
    var body: some View {
        
        HStack {
                        
            Text(occasion.name)
                // This line is Gold!
                // Expands the text and aligns it to the left, in order to accomplish the side effect of
                // making the whole line "tappable". Setting the background color is necessary!
                .frame(maxWidth: .infinity, alignment: .leading)
                .background(Color(.systemBackground))
                        
            //if occasion.name == selectedOccasionName {
            //    Image(systemName: "checkmark")
            //        .foregroundColor(.secondary)
            //}
            
        }
        
        //.navigationBarTitle("Occasion")
        
//        .onTapGesture {
//            //self.selectedOccasionName = self.occasion.name
//            self.presentationMode.wrappedValue.dismiss()
//            swiftyBeaverLog.debug("Cell tapped")
//        }
        
    } //: Body
}

// MARK: - Previews

//struct OccasionListCell_Previews: PreviewProvider {
//    static var previews: some View {
//        let occasion = Occasion(dictionary: ["Some occasion": "Anything"])
//        OccasionListCell(occasion: occasion)
//    }
//}
