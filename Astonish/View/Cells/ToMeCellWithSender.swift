//
//  ToMeCellWithSender.swift
//  Astonish
//
//  Created by Øystein Günther on 28/01/2021.
//

import SwiftUI
import struct Kingfisher.KFImage

struct ToMeCellWithSender: View {
    
    // MARK: - Properties
    
    let toMe: ToMe
    
    // MARK: - Body
    
    var body: some View {
        HStack {
            
            if let giftImageUrl = toMe.image?.url, let url = URL(string: giftImageUrl) {
                KFImage(url)
                    .resizable()
                    .scaledToFill()
                    .clipped()
                    .frame(width: PROFILE_IMAGE_WIDTH, height: PROFILE_IMAGE_HEIGHT)
                    //.cornerRadius(PROFILE_IMAGE_CORNER_RADIUS)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.white, lineWidth: 1))
            } else {
                Image(systemName: "gift")
                    .resizable()
                    .frame(width: PROFILE_IMAGE_WIDTH, height: PROFILE_IMAGE_HEIGHT, alignment: .center)
                    .font(Font.title.weight(.ultraLight))
                    .scaleEffect(0.7)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.white, lineWidth: 1))
            }
            
            VStack(alignment: .leading, spacing: 4) {
                Text(toMe.name)
                
                if toMe.senders?.count ?? 0 > 1 {
                    Text("Multiple")
                        .font(.footnote)
                } else {
                    Text(toMe.sender?.fullName ?? "")
                        .font(.footnote)
                }
                
            } //: VStack
            
            Spacer()
            
        } //: HStack
    } //: Body
}

/*
struct ToMeCellWithSender_Previews: PreviewProvider {
    static var previews: some View {
        let sender = Sender(id: "1", firstName: "Julie", lastName: "Günther")
        let occasion = Occasion(id: "1", name: "A great occasion")
        let toMe = ToMe(uid: "1", occasion: occasion, name: "Cool knife", sender: sender, note: "Love it", image: nil)
        ToMeCellWithSender(toMe: toMe)
            .previewLayout(.sizeThatFits)
            .previewDevice("iPhone 11 Pro")
    }
}
*/
