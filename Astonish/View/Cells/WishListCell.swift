//
//  WishListCell.swift
//  Astonish
//
//  Created by Øystein Günther on 02/02/2021.
//

import SwiftUI

struct WishListCell: View {
    
    // MARK: - Properties
    
    let wishList: WishList
    
    // MARK: - Body
    
    var body: some View {
        HStack {
                
            VStack(alignment: .leading, spacing: 4) {
                Text(wishList.name)
                    //.foregroundColor(.white)
                
            } //: VStack
        
            Spacer()
            
        } //: HStack
        //.background(Color(.gray))
        
    } //: Body
}

/*
struct WishListCell_Previews: PreviewProvider {
    static var previews: some View {
        WishListCell()
    }
}
*/
