//
//  AddWishListView.swift
//  Astonish
//
//  Created by Øystein Günther on 18/12/2020.
//

import SwiftUI

struct AddWishListView: View {
    
    // MARK: - Properties
    
    @State private var name: String = ""
    @Environment(\.presentationMode) var presentationMode
    
    // MARK: - Body
    
    var body: some View {
        NavigationView {
            VStack {
                
            } //: VStack
            .navigationBarTitle("New Wishlist", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        Text("Cancel")
                                    }, trailing:
                                        Button(action: {
                                            self.presentationMode.wrappedValue.dismiss()
                                        }) {
                                            Text("Save")
                                        }
            )
            
        } //: NavigationView
    }
}

// MARK: - Preview

struct AddWishListView_Previews: PreviewProvider {
    static var previews: some View {
        AddWishListView()
    }
}
