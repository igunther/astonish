//
//  EditWishListView.swift
//  Astonish
//
//  Created by Øystein Günther on 04/02/2021.
//

import SwiftUI

struct EditWishListView: View {
    
    // MARK: - Properties
    
    var wishList: WishList
    
    @Environment(\.presentationMode) var presentationMode
    @State private var name: String = ""
    @State private var selectedOccasionName: String = ""
    @State private var selectedOccasionId: String = ""
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                GroupBox {
                    Divider().padding(.vertical, 4)
                    
                    VStack {
                        TextField(wishList.name, text: $name)
                        Divider().padding(.vertical, 4)
                        
                        HStack {
                            NavigationLink(
                                destination: OccasionSelectView(selectedOccasionId: self.$selectedOccasionId, selectedOccasionName: self.$selectedOccasionName),
                                label: {
                                    VStack(alignment: .leading) {
                                        Text("Occasion")
                                            .font(Font.caption.weight(.light))
                                        
                                        HStack {
                                            Text(selectedOccasionName.isEmpty ? "Mandatory" : selectedOccasionName)
                                                .foregroundColor(Color(.systemGray2))
                                            Spacer()
                                            Image(systemName: "chevron.right")
                                                .foregroundColor(Color(.systemGray2))
                                        } //: HStack
                                    } //: VStack
                                })
                        } //: HStack
                        
                        Divider().padding(.vertical, 4)
                        
                    } //: VStack
                } //: GroupBox
                .padding()
                
            } //: ScrollView
                        
            .navigationBarTitle("Edit Wishlist", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        Text("Cancel")
                                    }, trailing:
                                        Button(action: {
                                            //addGift()
                                            self.presentationMode.wrappedValue.dismiss()
                                        }) {
                                            Text("Save")
                                        }
            ) //: NavigationBarItems
            
        } //: NavigationView
    } //: Body
}

struct EditWishListView_Previews: PreviewProvider {
    static var previews: some View {
        
        let recipient = Recipient(id: "1", firstName: "Julie", lastName: "Günther")
        let occasion = Occasion(id: "1", name: "Christmas 2020")
        let wishList = WishList(id: "1", uid: "2", recipient: recipient, occasion: occasion, name: "2")
        
        EditWishListView(wishList: wishList)
    }
}
