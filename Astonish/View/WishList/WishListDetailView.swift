//
//  WishListDetailView.swift
//  Astonish
//
//  Created by Øystein Günther on 02/02/2021.
//

import SwiftUI

struct WishListDetailView: View {
    
    // MARK: - Properties
    
    var wishList: WishList
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var wishesViewModel = WishesViewModel()
    
    @State private var activeSheet: ActiveSheet?
    
    var body: some View {
        //NavigationView {
            ZStack(alignment: .bottomTrailing) {
                
                // MARK: - Section 1
                
                List(wishesViewModel.wishes ) { wish in
                    WishCell(wish: wish)
                        .onTapGesture { self.selectDeselect(wish) }
                }
                //.listStyle(InsetGroupedListStyle())
                .listStyle(PlainListStyle())
                                
                
                RoundActionButton(systemImage: "heart") {
                    //self.showingAddWishView.toggle()
                }
            }
        
        
        .onAppear() {
            self.wishesViewModel.get(wishListId: wishList.id)
        }
                
        .toolbar {
            
            ToolbarItem(placement: .principal) {
                Text(wishList.name)
            }
            
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(action: {
                    activeSheet = .second
                }) {
                    Text("Edit")
                }
                
            } //: ToolbarItem
            
        } //: .toolbar
        
        .sheet(item: $activeSheet) { item in
            switch item {
            case .first:
                SettingsView()
            case .second:
                EditWishListView(wishList: wishList)
            }
            
        } //: .sheet
        
        } //: ZStack
    //} //: NavigationView
    
    private func selectDeselect(_ wish: Wish) {
        print("selected \(wish.name)")
    }
    
} //: Body


struct WishListDetailView_Previews: PreviewProvider {
    static var previews: some View {
        
        let recipient = Recipient(id: "1", firstName: "Julie", lastName: "Günther")
        let occasion = Occasion(id: "1", name: "Christmas 2020")
        let wishList = WishList(id: "1", uid: "2", recipient: recipient, occasion: occasion, name: "2")
        
        WishListDetailView(wishList: wishList)
    }
}

