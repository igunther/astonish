//
//  AddPersonView.swift
//  Astonish
//
//  Created by Øystein Günther on 30/12/2020.
//

import SwiftUI

struct AddPersonView: View {
    
    // MARK: - Properties
    
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var birthDate = Date()
    @State private var groupName: String = ""
    @State private var groupId: String = ""
    @State private var phone: String = ""
    @State private var eMail: String = ""
    @State private var address: String = ""
    @State private var showImagePicker = false
    @State private var image: Image?
    @State private var selectedUIImage: UIImage?
    @State private var showActionSheet = false
    @State private var sourceType: UIImagePickerController.SourceType = .camera
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var viewModel = PeopleViewModel()
    
    func loadImage() {
        guard let selectedImage = selectedUIImage else {  return }
        image = Image(uiImage: selectedImage)
    }
    
    // MARK: - Body
    
    var body: some View {
        NavigationView {
            
            VStack {
                Button(action: { showActionSheet.toggle() }, label: {
                    ZStack {
                        if let image = image {
                            image
                                .resizable()
                                .scaledToFill()
                                .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X)
                                .clipped()
                                .cornerRadius(PROFILE_IMAGE_CORNER_RADIUS_2X)
                                .padding(.top, 30)
                        } else {
                            Image(systemName: "person.crop.circle")
                                .resizable()
                                .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X, alignment: .center)
                                .font(Font.title.weight(.light))
                                .padding(.top, 30)
                        }
                    } //: ZStack
                })
                
                .sheet(isPresented: $showImagePicker, onDismiss: loadImage, content: {
                    ImagePicker(image: $selectedUIImage, sourceType: self.sourceType)
                })
                
                .actionSheet(isPresented: $showActionSheet, content: {
                    ActionSheet(title: Text("Select Photo"), buttons: [.default(Text("Photo Library")) {
                        self.showImagePicker = true
                        self.sourceType = .photoLibrary
                    },
                    .default(Text("Camera")) {
                        #if targetEnvironment(simulator)
                        #else
                            self.showImagePicker = true
                            self.sourceType = .camera
                        #endif
                    },
                    .cancel()])
                })
                
                Form {
                    
                    Section {
                        
                        TextField("First Name", text: $firstName)
                        TextField("Last Name", text: $lastName)
                        DatePicker(selection: $birthDate, in: ...Date(), displayedComponents: .date) {
                            Text("Select a date")
                        }
                    }
                    
                    Section {
                        TextField("Group", text: $groupName)
                    }
                    
                    Section {
                        TextField("Phone", text: $phone)
                        TextField("EMail", text: $eMail)
                        TextField("Address", text: $address)
                    }
                    
                } //: Form
                
            } //: VStack
            
            .navigationBarTitle("New Person", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        Text("Cancel")
                                    }, trailing:
                                        Button(action: {
                                            guard let uid = authViewModel.userSession?.uid else { return }
                                            self.viewModel.createPerson(uid: uid, firstName: firstName, lastName: lastName, group: groupName, image: selectedUIImage, email: eMail)
                                            self.presentationMode.wrappedValue.dismiss()
                                        }) {
                                            Text("Save")
                                        }
            ) //: navigationBarItems
            
        } //: NavigationView
    } //: Body
}

// MARK: - Previews

struct AddPerson_Previews: PreviewProvider {
    static var previews: some View {
        AddPersonView()
    }
}
