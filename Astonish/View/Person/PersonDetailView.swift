//
//  PersonDetailView.swift
//  Astonish
//
//  Created by Øystein Günther on 01/01/2021.
//

import SwiftUI
import struct Kingfisher.KFImage

struct PersonDetailView: View {
    
    // MARK: - Properties
    
    let person: Person
    
    @State private var image: Image?
    @State private var selectedUIImage: UIImage?
    @State private var showActionSheet = false
    @State private var showImagePicker = false
    @State private var sourceType: UIImagePickerController.SourceType = .camera
    
    @State private var firstName: String = ""
    @State private var lastName: String = ""
    @State private var birthDate = Date()
    @State private var group: String = ""
    //@State private var groupId: String = ""
    @State private var phone: String = ""
    @State private var email: String = ""
    @State private var address: String = ""
    
    @State private var profileImageUrl: String?
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var authViewModel: AuthViewModel
    
    @StateObject var viewModel = PeopleViewModel()
    @ObservedObject var giftViewModel = GiftViewModel()
    
    @State private var isAnimating: Bool = false
    
    @State private var page: String = "Profile"
    let pages = ["Profile", "Gifts"]
    
    func loadImage() {
        guard let selectedImage = selectedUIImage else {  return }
        image = Image(uiImage: selectedImage)
    }
    
    // MARK: - Body
    
    var body: some View {
        
        Picker("Profile", selection: $page) {
            ForEach(pages, id: \.self) {
                Text($0)
            }
        }
        .pickerStyle(SegmentedPickerStyle())
        .padding()
        
        if page == "Profile" {
            
            ScrollView(.vertical, showsIndicators: false) {
                
                VStack(spacing: 20) {
                    
                    // MARK: - Section 1
                    // XXX START
                    GroupBox(
                        label:
                            Text("Basic")
                        //.frame(width: 0, height: 0
                        //                        SettingsLabelView(labelText: "Fructus", labelImage: "info.circle")
                    ) {
                        Divider().padding(.vertical, 4)
                        
                        HStack(alignment: .center, spacing: 10) {
                            
                            Button(action: { showActionSheet.toggle() }, label: {
                                
                                ZStack {
                                    if let image = image {
                                        image
                                            .resizable()
                                            .scaledToFill()
                                            .clipped()
                                            .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X)
                                            .clipShape(Circle())
                                            .overlay(Circle().stroke(Color.white, lineWidth: 1))
                                            //.padding()
                                            .scaleEffect(isAnimating ? 1.0 : 0.6)
                                    }
                                    else if let profileImageUrl = profileImageUrl, let url = URL(string: profileImageUrl) {
                                        KFImage(url)
                                            .resizable()
                                            .scaledToFill()
                                            .clipped()
                                            .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X)
                                            .clipShape(Circle())
                                            .overlay(Circle().stroke(Color.white, lineWidth: 1))
                                            //.padding()
                                            .scaleEffect(isAnimating ? 1.0 : 0.6)
                                    }
                                    else {
                                        Image(systemName: "person.crop.circle")
                                            .resizable()
                                            .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X, alignment: .center)
                                            .font(Font.title.weight(.ultraLight))
                                            //.padding()
                                            .scaleEffect(isAnimating ? 1.0 : 0.6)
                                    }
                                } //: ZStack
                                
                            }) //: Button
                            .onAppear {
                                profileImageUrl = person.profileImageUrl
                                withAnimation(.easeOut(duration: 1.0)) {
                                    isAnimating = true
                                }
                            }
                            
                            VStack {
                                TextField(person.firstName ?? "", text: $firstName)
                                //    .background(Color(.red))
                                Divider().padding(.vertical, 4)
                                TextField((person.lastName ?? "").isEmpty ? "Last Name" : person.lastName!, text: $lastName)
                                //    .background(Color(.red))
                                
                                Divider().padding(.vertical, 4)
                                TextField((person.group ?? "").isEmpty ? "Group" : person.group!, text: $group)
                                
                                //HStack {
                                //                                Text("Group")
                                //                                    .background(Color(.red))
                                //Spacer()
                                //TextField((person.group ?? "").isEmpty ? "Group" : person.group!, text: $group)
                                ///     .background(Color(.green))
                                //Divider().padding(.vertical, 4)
                                //}
                                
                                
                            } //: VStack
                        } //: HStack
                    } //: GroupBox
                    // XXX END
                    
                    // MARK: - Section 2
                    /// BBB START
                    Group{
                        DatePicker(selection: $birthDate, in: ...Date(), displayedComponents: .date) {
                            Text("Birthdate")
                        }
                        Divider().padding(.vertical, 4)
                        TextField("Phone", text: $phone)
                        Divider().padding(.vertical, 4)
                        TextField("EMail", text: $email)
                        Divider().padding(.vertical, 4)
                        TextField("Address", text: $address)
                        Divider().padding(.vertical, 4)
                    }
                    
                    .sheet(isPresented: $showImagePicker, onDismiss: loadImage, content: {
                        ImagePicker(image: $selectedUIImage, sourceType: self.sourceType)
                    })
                    
                    .actionSheet(isPresented: $showActionSheet, content: {
                        ActionSheet(title: Text("Photo"), buttons: [
                            .default(Text("Photo Library")) {
                                self.showImagePicker = true
                                self.sourceType = .photoLibrary
                            },
                            .default(Text("Camera")) {
                                #if targetEnvironment(simulator)
                                #else
                                self.showImagePicker = true
                                self.sourceType = .camera
                                #endif
                            },
                            .default(Text("Delete")) {
                                StorageImageHelper.deleteImage(documentId: person.profileImageId) { success in
                                    if success {
                                        viewModel.removeProfileImageFromPerson(id: person.id)
                                        profileImageUrl = nil
                                    }
                                }
                            }
                            ,
                            .cancel()
                        ])
                    })
                    // BBB END
                    
                } //: VStack
                
                .navigationBarItems( trailing:
                                        Button(action: {
                                            updatePerson()
                                            self.presentationMode.wrappedValue.dismiss()
                                        }) {
                                            Text("Save")
                                        }
                ) //: navigationBarItems
                
                .padding()
                
            } //: ScrollView
            
        } else { //: page ==
            
            List(giftViewModel.gifts) { gift in
                //NavigationLink(destination: GiftDetailView(gift: gift)) {
                    GiftCellWithState(gift: gift)
                //}
            }
            //.listStyle(InsetGroupedListStyle())
            
            .onAppear() {
                self.giftViewModel.get(recipientId: person.id)
                swiftyBeaverLog.debug("Done calling fetchGifts.")
            }
            
            .navigationBarItems( trailing:
                                    Button(action: {
                                    }) {
                                        Text("")
                                    }
            ) //: navigationBarItems
        }
    } //: Body
    
    // MARK: - Functions
    
    private func updatePerson() {
        guard let uid = authViewModel.userSession?.uid else { return }
        viewModel.updatePerson(uid: uid,
                               id: person.id,
                               firstName: firstName == "" ? person.firstName ?? "" : firstName,
                               lastName: lastName == "" ? person.lastName ?? "" : lastName,
                               group: group == "" ? person.group ?? "" : group,
                               email: email == "" ? person.email : email,
                               image: selectedUIImage,
                               profileImageId: person.profileImageId)
    }
}

// MARK: - Preview

struct PersonDetailView_Previews: PreviewProvider {
    static var previews: some View {
        
        let personData = ["id": "1", "firstName": "Erlich"]
        let person = Person(dictionary: personData)
        
        PersonDetailView(person: person)
    }
}
