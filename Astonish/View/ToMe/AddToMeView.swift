//
//  AddToMeView.swift
//  Astonish
//
//  Created by Øystein Günther on 28/01/2021.
//

import SwiftUI
import struct Kingfisher.KFImage

struct AddToMeView: View {
    
    // MARK: - Properties
        
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var toMeViewModel = ToMeViewModel()
    @StateObject var peopleViewModel = PeopleViewModel()
    
    @State private var name: String = ""
    @State private var selectedOccasionName: String = ""
    @State private var selectedOccasionId: String = ""
    @State private var image: Image?
    @State private var selectedUIImage: UIImage?
    @State private var showActionSheet = false
    @State private var showImagePicker = false
    @State private var sourceType: UIImagePickerController.SourceType = .camera
    @State private var giftImageUrl: String = ""
    @State var selectedSenders = Set<Sender>()
    @State private var quantity = NumbersOnly()
    @State private var note: String = ""
    @State private var isAnimating: Bool = false
            
    func loadImage() {
        guard let selectedImage = selectedUIImage else {  return }
        image = Image(uiImage: selectedImage)
    }
        
    // MARK: - Body
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(spacing: 20) {
                    
                    // MARK: - Section 1
                    
                    GroupBox {
                        Divider().padding(.vertical, 4)
                        
                        HStack(alignment: .center, spacing: 10) {
                            
                            Button(action: { showActionSheet.toggle() }, label: {
                                
                                ZStack {
                                    if let image = image {
                                        image
                                            .resizable()
                                            .scaledToFill()
                                            .clipped()
                                            .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X)
                                            .clipShape(Circle())
                                            .overlay(Circle().stroke(Color.white, lineWidth: 1))
                                    }
                                    else if let giftImageUrl = giftImageUrl, let url = URL(string: giftImageUrl) {
                                        KFImage(url)
                                            .resizable()
                                            .scaledToFill()
                                            .clipped()
                                            .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X)
                                            .clipShape(Circle())
                                            .overlay(Circle().stroke(Color.white, lineWidth: 1))
                                            //.padding()
                                            .scaleEffect(isAnimating ? 1.0 : 0.6)
                                    }
                                    else {
                                        Image(systemName: "gift")
                                            .resizable()
                                            .frame(width: PROFILE_IMAGE_WIDTH_2X, height: PROFILE_IMAGE_HEIGHT_2X, alignment: .center)
                                            .font(Font.title.weight(.ultraLight))
                                            .scaleEffect(0.7)
                                            .clipShape(Circle())
                                            .overlay(Circle().stroke(Color.white, lineWidth: 1))
                                    }
                                } //: ZStack
                            }) //: Button
                            .onAppear {
                                withAnimation(.easeOut(duration: 1.0)) {
                                    isAnimating = true
                                }
                            }
                            
                            VStack {
                                TextField("Name", text: $name)
                                Divider().padding(.vertical, 4)
                                
                                HStack {
                                    NavigationLink(
                                        destination: OccasionSelectView(selectedOccasionId: self.$selectedOccasionId, selectedOccasionName: self.$selectedOccasionName),
                                        label: {
                                            VStack(alignment: .leading) {
                                                Text("Occasion")
                                                    .font(Font.caption.weight(.light))
                                                
                                                HStack {
                                                    Text(selectedOccasionName.isEmpty ? "Mandatory" : selectedOccasionName)
                                                        .foregroundColor(Color(.systemGray2))
                                                    Spacer()
                                                    Image(systemName: "chevron.right")
                                                        .foregroundColor(Color(.systemGray2))
                                                } //: HStack
                                            } //: VStack
                                        })
                                } //: HStack
                                
                                Divider().padding(.vertical, 4)
                                
                            } //: VStack
                        } //: HStack
                    } //: GroupBox
                    
                    // MARK: - Section 2
                    
                    Group {
                        
                        HStack {
                            
                            NavigationLink(
                                destination: SenderSelectView(selectedSenders: $selectedSenders),
                                label: {
                                    Text("Sender")
                                        .frame(maxWidth: .infinity, alignment: .leading)
                                        .background(Color(.systemBackground))
                                    
                                    Spacer()
                                    
                                    if selectedSenders.count == 0 {
                                        Text("Optional")
                                            .foregroundColor(Color(.systemGray2))
                                    } else if selectedSenders.count == 1 {
                                        if let sender = selectedSenders.first {
                                            Text(sender.fullName)
                                        }
                                    } else {
                                        Text("Multiple...")
                                    }
                                    
                                    Image(systemName: "chevron.right")
                                        .foregroundColor(Color(.systemGray2))
                                    
                                }) //: NavigationLink
                        } //: HStack
                        
                        Divider().padding(.vertical, 4)
                        
                        HStack {
                            Text("Quantity")
                                .background(Color(.systemBackground))
                            
                            Spacer()
                            
                            TextField("", text: quantity.value == "" ? .constant("1") : $quantity.value)
                                .keyboardType(.decimalPad)
                                .frame(maxWidth: .infinity, alignment: .trailing)
                                .multilineTextAlignment(.trailing)
                        }
                        
                        Divider().padding(.vertical, 4)
                        
                        VStack(alignment: .leading) {
                            
                            Text("Notes")
                                .font(Font.caption.weight(.light))
                            
                            TextEditor(text: $note)
                                .frame(height: 150)
                        }
                        
                        Divider().padding(.vertical, 4)
                    }
                    
                    .sheet(isPresented: $showImagePicker, onDismiss: loadImage, content: {
                        ImagePicker(image: $selectedUIImage, sourceType: self.sourceType)
                    })
                    
                    .actionSheet(isPresented: $showActionSheet, content: {
                        ActionSheet(title: Text("Photo"), buttons: [
                            .default(Text("Photo Library")) {
                                self.showImagePicker = true
                                self.sourceType = .photoLibrary
                            },
                            .default(Text("Camera")) {
                                #if targetEnvironment(simulator)
                                #else
                                self.showImagePicker = true
                                self.sourceType = .camera
                                #endif
                            },
                            .cancel()
                        ])
                    })
                } //: VStack
                
            } //: ScrollView
            .padding()
            
            .navigationBarTitle("New Gift To me", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        Text("Cancel")
                                    }, trailing:
                                        Button(action: {
                                            addToMe()
                                            self.presentationMode.wrappedValue.dismiss()
                                        }) {
                                            Text("Save")
                                        }.disabled(name == "" || selectedOccasionId == "")
            ) //: NavigationBarItems
            
            .onAppear() {
                
            }
        } //: NavigationView
    } //: Body
    
    // MARK: - Functions
    
    private func addToMe() {
        guard let uid = authViewModel.userSession?.uid else { return }
                
        let occasion = Occasion(id: selectedOccasionId, name: selectedOccasionName)
        let toMe = ToMe(uid: uid, occasion: occasion, name: name, senders: selectedSenders, quantity: quantity.value, note: note)
        
        toMeViewModel.add(toMe: toMe, giftImage: selectedUIImage)
    }
    
}

/*
struct AddToMeView_Previews: PreviewProvider {
    static var previews: some View {
        AddToMeView()
    }
}
*/
