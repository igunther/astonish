//
//  AddOccasionView.swift
//  Astonish
//
//  Created by Øystein Günther on 25/12/2020.
//

import SwiftUI

struct AddOccasionView: View {
    
    // MARK: - Properties
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var viewModel = OccasionViewModel()
    
    @State private var name: String = ""
    @State private var occasionDate = Date()
    @ObservedObject private var budget = NumbersOnly()
    @State private var note: String = ""
    
    // MARK: - Body
    
    var body: some View {
        NavigationView {
            
            Form {
                
                TextField("Name", text: $name)
                
                DatePicker("Date", selection: $occasionDate, displayedComponents: .date)
                
                TextField("Budget", text: $budget.value)
                    .keyboardType(.decimalPad)
                
                ZStack(alignment: .leading) {
                    if note.isEmpty {
                        Text("Notes")
                    }
                    TextEditor(text: $note)
                }
                
            } //: Form
            .navigationBarTitle("New Occasion", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button(action: {
                                        self.presentationMode.wrappedValue.dismiss()
                                    }) {
                                        Text("Cancel")
                                    }, trailing:
                                        Button(action: {
                                            viewModel.createOccasion(uid: authViewModel.userSession?.uid, name: name)
                                            self.presentationMode.wrappedValue.dismiss()
                                        }) {
                                            Text("Save")
                                        }
            )
            
        } //: NavigationView
    }
}

// MARK: - Preview

struct AddOccasionView_Previews: PreviewProvider {
    static var previews: some View {
        AddOccasionView()
    }
}
