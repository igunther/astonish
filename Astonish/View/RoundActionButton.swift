//
//  RoundActionButton.swift
//  Astonish
//
//  Created by Øystein Günther on 18/12/2020.
//

import SwiftUI

struct RoundActionButton: View {
    
    // MARK: - Properties
    
    let systemImage: String
    let action: () -> Void
        
    init(systemImage: String, action: @escaping () -> Void) {
        self.systemImage = systemImage
        self.action = action
    }
    
    // MARK: - Body
    
    var body: some View {
        Button(action: action) {
            Image(systemName: systemImage)
                .frame(width: 36, height: 36)
                .font(.title)
                .padding()
        }
        .background(Color(.systemGray4))
        .mask(Circle())
        .padding()
    }
}

// MARK: - Preview

struct RoundActionButton_Previews: PreviewProvider {
    static var previews: some View {
        RoundActionButton(systemImage: "calendar", action: {})
    }
}
