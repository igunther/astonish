//
//  SenderSelectView.swift
//  Astonish
//
//  Created by Øystein Günther on 29/01/2021.
//

import SwiftUI

struct SenderSelectView: View {
    
    // MARK: - Properties
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var authViewModel: AuthViewModel
    @State private var showingSheet = false
    @State private var showingAddPersonView: Bool = false
    @StateObject var peopleViewModel = PeopleViewModel()
    @State var selectedRows = Set<Sender>()
    @Binding var selectedSenders: Set<Sender>
    
    // MARK: - Body
    
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            VStack(alignment: .leading) {
                List(peopleViewModel.senders, selection: $selectedRows ) { sender in
                    SenderCell(sender: sender, selectedItems: self.$selectedRows)
                }
                //.listStyle(InsetGroupedListStyle())
                .listStyle(PlainListStyle())
            } //: VStack
        }
        .sheet(isPresented: $showingAddPersonView, content: {
            AddPersonView()
        })
        
        .onAppear() {
            // If we are returning from the AddGiftView once again, then we need to make sure that
            // any previously selected recipients will still be selected (visualized by a checkmark).
            self.selectedRows = selectedSenders
            self.peopleViewModel.getSenders(uid: authViewModel.userSession?.uid)
        }
        .navigationBarItems(trailing:
                                Button(action: {
                                    // Update the binding variable with selections made.
                                    self.selectedSenders = selectedRows
                                    self.presentationMode.wrappedValue.dismiss()
                                }) {
                                    Text("Done")
                                }
        )
    } //: Body
}

/*
struct SenderSelectView_Previews: PreviewProvider {
    static var previews: some View {
 SenderSelectView()
    }
}
*/
