//
//  OccasionSelectView.swift
//  Astonish
//
//  Created by Øystein Günther on 29/12/2020.
//

import SwiftUI

struct OccasionSelectView: View {
    
    // MARK: - Properties
    
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var occasionViewModel = OccasionViewModel()
    @Binding var selectedOccasionId: String
    @Binding var selectedOccasionName: String
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    // MARK: - Body
    
    var body: some View {
        VStack(alignment: .leading) {
            List(occasionViewModel.occasions) { occasion in
                OccasionListCell(occasion: occasion)
                    .onTapGesture {
                        swiftyBeaverLog.debug("Cell tapped")
                        self.selectedOccasionName = occasion.name
                        self.selectedOccasionId = occasion.id
                        self.presentationMode.wrappedValue.dismiss()
                    }
                
                if selectedOccasionName == occasion.name {
                    
                        Image(systemName: "checkmark")
                            .foregroundColor(.secondary)
                }
            } //: List
        }
        
        .onAppear() {
            self.occasionViewModel.get(uid: authViewModel.userSession?.uid)
        }
    } //: Body
}

// MARK: - Preview

//struct OccasionSelectView_Previews: PreviewProvider {
//    static var previews: some View {
//        OccasionSelectView(occasionId: .constant(""))
//    }
//}
