//
//  RecipientSelectView.swift
//  Astonish
//
//  Created by Øystein Günther on 08/01/2021.
//

import SwiftUI

struct RecipientSelectView: View {
    
    // MARK: - Properties
    
    @Environment(\.presentationMode) var presentationMode
    @State private var showingSheet = false
    @State private var showingAddPersonView: Bool = false
    @EnvironmentObject var authViewModel: AuthViewModel
    @StateObject var peopleViewModel = PeopleViewModel()
    @State var selectedRows = Set<Recipient>()
    @Binding var selectedRecipients: Set<Recipient>
    
    // MARK: - Body
    
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            VStack(alignment: .leading) {
                List(peopleViewModel.recipients, selection: $selectedRows ) { recipient in
                    RecipientCell(recipient: recipient, selectedItems: self.$selectedRows)
                }
                //.listStyle(InsetGroupedListStyle())
                .listStyle(PlainListStyle())
            } //: VStack
        }
        .sheet(isPresented: $showingAddPersonView, content: {
            AddPersonView()
        })
        
        .onAppear() {
            // If we are returning from the AddGiftView once again, then we need to make sure that
            // any previously selected recipients will still be selected (visualized by a checkmark).
            self.selectedRows = selectedRecipients
            self.peopleViewModel.getRecipients(uid: authViewModel.userSession?.uid)
        }
        .navigationBarItems(trailing:
                                Button(action: {
                                    // Update the binding variable with selections made.
                                    self.selectedRecipients = selectedRows
                                    self.presentationMode.wrappedValue.dismiss()
                                }) {
                                    Text("Done")
                                }
        )
    } //: Body
}

//struct RecipientSelectView_Previews: PreviewProvider {
//    static var previews: some View {
//        RecipientSelectView()
//    }
//}
