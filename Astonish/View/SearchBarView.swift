//
//  SearchBarView.swift
//  Astonish
//
//  Created by Øystein Günther on 18/12/2020.
//

import SwiftUI

struct SearchBarView: View {
    
    // MARK: - Properties
    
    @Binding var text: String
    
    // MARK: - Body
    
    var body: some View {
        HStack {
            TextField("Search...", text: $text)
                .padding(8)
                .padding(.horizontal, 24)
                .background(Color(.systemGray6))
                .cornerRadius(8)
                .overlay(
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.gray)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 8)
                    } //: HStack
                )
        } //: HStack
    }
}

// MARK: - Preview

struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        SearchBarView(text: .constant("Search..."))
    }
}
